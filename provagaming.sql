-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 16, 2018 alle 21:43
-- Versione del server: 10.1.28-MariaDB
-- Versione PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `provagaming`
--
CREATE DATABASE IF NOT EXISTS `provagaming` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `provagaming`;

-- --------------------------------------------------------

--
-- Struttura della tabella `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `GameId` int(4) NOT NULL,
  `Name` varchar(160) NOT NULL,
  `Path` varchar(160) NOT NULL,
  `valutazione` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `game`
--

INSERT INTO `game` (`GameId`, `Name`, `Path`, `valutazione`) VALUES
(0, 'Breakout', '', 2.8),
(1, 'Collision', '', 4.1),
(2, 'SpaceInvaders', '', 3.2),
(3, 'Tetris', '', 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `ReviewId` int(11) NOT NULL,
  `Title` varchar(160) NOT NULL,
  `GameId` int(4) DEFAULT NULL,
  `Author` varchar(160) DEFAULT NULL,
  `Testo` varchar(500) DEFAULT NULL,
  `Voto` int(10) DEFAULT NULL,
  `Approved` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `timeline`
--

DROP TABLE IF EXISTS `timeline`;
CREATE TABLE `timeline` (
  `GamerId` varchar(160) NOT NULL,
  `GameId` int(11) NOT NULL,
  `LivelloConquistato` int(100) DEFAULT NULL,
  `Giorno` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `timeline`
--

INSERT INTO `timeline` (`GamerId`, `GameId`, `LivelloConquistato`, `Giorno`) VALUES
('ciao', 0, 10, '16/2/2018'),
('ciao', 0, 10, '16/2/2018'),
('ciao', 1, 290, '16/2/2018'),
('ciao', 2, 0, '16/2/2018'),
('ciao', 3, 0, '16/2/2018'),
('rec', 0, 10, '16/2/2018'),
('rec', 1, 230, '16/2/2018'),
('rec', 2, 200, '16/2/2018'),
('rec', 3, 0, '16/2/2018'),
('rec', 3, 0, '16/2/2018'),
('rec', 0, 0, '16/2/2018'),
('rec', 1, 330, '16/2/2018'),
('rec', 3, 0, '16/2/2018');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `email` varchar(160) NOT NULL,
  `name` varchar(160) NOT NULL,
  `surname` varchar(160) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `livello` int(11) DEFAULT NULL,
  `role` tinyint(1) DEFAULT NULL,
  `experience` int(11) DEFAULT NULL,
  `password` varchar(160) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`email`, `name`, `surname`, `username`, `livello`, `role`, `experience`, `password`) VALUES
('ciao', 'fra', 'fra', 'Nfre', 0, 0, 300, 'ciao'),
('fra@live.it', 'Nome', 'Cognome', 'Nick', 0, 1, 0, 'casa'),
('frizz96', 'fabrizio', 'franchitti', 'frizz', 0, 1, 0, 'ciao'),
('grecosandro@outlook.com', 'Sandro', 'Greco', 'PizzaGameBoy91', 0, 1, 0, 'casa'),
('rec', 'rec', 'rec', 'rec', 0, 0, 1640, 'rec'),
('sara@live.it', 'sara', 'di naccio', 'sara66', 0, 0, 0, 'sara'),
('see', 'see2', 'see', 'see', 0, 2, 0, 'see'),
('sheldon@live.it', 'Sheldon', 'Cooper', 'Sheldor86', 0, 2, 0, 'sheldon');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`GameId`);

--
-- Indici per le tabelle `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`ReviewId`),
  ADD KEY `GameId` (`GameId`),
  ADD KEY `Author` (`Author`);

--
-- Indici per le tabelle `timeline`
--
ALTER TABLE `timeline`
  ADD KEY `GamerId` (`GamerId`),
  ADD KEY `GameId` (`GameId`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `review`
--
ALTER TABLE `review`
  MODIFY `ReviewId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`GameId`) REFERENCES `game` (`GameId`) ON DELETE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`Author`) REFERENCES `users` (`email`) ON DELETE CASCADE;

--
-- Limiti per la tabella `timeline`
--
ALTER TABLE `timeline`
  ADD CONSTRAINT `timeline_ibfk_1` FOREIGN KEY (`GamerId`) REFERENCES `users` (`email`) ON DELETE CASCADE,
  ADD CONSTRAINT `timeline_ibfk_2` FOREIGN KEY (`GameId`) REFERENCES `game` (`GameId`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
