package interfaces;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public interface GameInterface {
	
	/**
	 * Basta stampare su console lo score finale.
	 * @return
	 */
	int sendExperience();

}
