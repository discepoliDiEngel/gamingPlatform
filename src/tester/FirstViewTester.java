package tester;

import view.FirstView;

/**
 * Classe da usare per testare il funzionamento della FirstView.
 * 
 * @author Discepoli di Engel
 *
 */
public class FirstViewTester {
	
	public static void main(String[] args) {
		new FirstView();
	}

}
