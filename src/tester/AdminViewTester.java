package tester;

import view.AdminView;

/**
 * Classe da usare per testare il funzionamento della AdminView.
 * 
 *@author Discepoli di Engel
 *
 */
public class AdminViewTester {
	
	public static void main(String[] args) {
		new AdminView();
	}

}
