package tester;

import model.User;
import view.GamerView;

/**
 * Classe da usare per testare il funzionamento della GamerView.
 * 
 * @author Discepoli di Engel
 *
 */
public class GamerViewTester {
	
	public static void main(String[] args) {
		User user = new User("Raji", "Koothrapali", "rajes@rules.com", "rajiKoot", 670, 0);
		new GamerView(user);
	}

}
