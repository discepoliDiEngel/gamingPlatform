package tester;

import model.User;
import view.ProfileView;

/**
 * Classe da usare per testare il funzionamento della ProfileView.
 * 
 * @author Discepoli di Engel
 *
 */
public class ProfileViewTester {

	public static void main(String[] args) {
		
		int exp = 850;
		int tipoUser = 0;
		User raji = new User("Rajesh", "Koothrapali", "raj@kooth.pali", "rajiko", exp, tipoUser);
		
		new ProfileView(raji, raji.getRole());
	}
	
}
