package tester;

import view.controller.ModeratorController;

/**
 * Classe da usare per testare il funzionamento della ModeratorView.
 * 
 * @author Discepoli di Engel
 *
 */
public class ModeratorViewTester {
	
	public static void main(String[] args) {
		new ModeratorController();
	}

}
