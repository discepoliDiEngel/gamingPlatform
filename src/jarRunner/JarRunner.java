package jarRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import model.Game;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class JarRunner {
	
	/**
	 * Esegue l'applicazione JAR data in input e aspetta che questa finisca l'esecuzione.
	 * 
	 * @param game Gioco che verra' eseguito
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 * @return int 
	 */	
	public static int launch(Game game) throws IOException, InterruptedException {
		
		int score = 0;
		ProcessBuilder pb = new ProcessBuilder("java", "-jar", game.getName() + ".jar");
		pb.redirectErrorStream(true);
		pb.directory(new File("games/"));

		System.out.println("Directory: " + pb.directory().getAbsolutePath());
		Process p = pb.start();
		InputStream is = p.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		
		// Lettura dalla console del processo mandato in esecuzione
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			score += Integer.parseInt(line);
		}
		
		p.waitFor();
		return score;

	}
	
}
