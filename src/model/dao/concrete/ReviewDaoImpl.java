package model.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;
import database.ConnectionFactory;
import java.sql.Connection;
import model.Game;
import model.Review;
import model.dao.interfaces.ReviewDao;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class ReviewDaoImpl implements ReviewDao {

    private PreparedStatement prStmt;
    private Connection con;
    
    public ReviewDaoImpl() {
    	con = new ConnectionFactory().getConnection();
    }

	@Override
	public boolean insertReview(String title, String author, String text, int vote, int gameID) {
        
        String insertReview = "INSERT INTO `review` (`Title`, `GameId`, `Author`, `Testo`, `Voto`, `Approved`) "
				+ "VALUES ('"+ title +"', '"+ gameID +"', '"+ author +"', '" + text +"', "+ vote +" , " + 0 + ");";

		try {

			prStmt = con.prepareStatement(insertReview);
			prStmt.executeUpdate(insertReview);

			System.out.println("Review salvata con successo.");
			prStmt.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;

	}

	@Override
	public TreeMap<String, Review> getReviewsByGame(Game gameObj) {
		
        TreeMap<String, Review> map = new TreeMap<String, Review>();
        
		String selectSQL = "SELECT * FROM review r INNER JOIN game g ON r.GameId = g.GameId WHERE r.Approved = '" + 1 + "' AND g.GameId = '" + gameObj.getId() + "';";

		try {

			Statement stmt = con.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery(selectSQL);

			while(rs.next()) {
				Review revTemp = getReviewFromResultSet(rs);
				map.put(revTemp.getTitle(), revTemp);
			}

			return map;

		} catch (SQLException e) {
			e.printStackTrace();
		}

        return null;
        
	}

	@Override
	public boolean approveReview(Review rev) {
        
        String updateSQL = "UPDATE review SET Approved = 1 WHERE Title = '" + rev.getTitle() + "' ;";

		try {

			prStmt = con.prepareStatement(updateSQL);
			prStmt.executeUpdate(updateSQL);

			System.out.println("Approvazione Review andata a buon fine.");
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Operazione non andata a buon fine.");
        return false;
        
	}

	@Override
	public boolean deleteReview(Review rev) {

		String deleteReview = "DELETE FROM review WHERE review.Title = '" + rev.getTitle() + "';";

		try {

			Statement prepStmt = con.prepareStatement(deleteReview);
			prepStmt.execute(deleteReview);

			System.out.println("Delete recensione avvenuta con successo.");
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

        return false;
        
	}

	@Override
	public TreeMap<String, Review> mapReview() {
        
        TreeMap<String, Review> treeRev = new TreeMap<String, Review>();
		String selectSQL = "SELECT * FROM review r INNER JOIN game g ON r.GameId = g.GameId WHERE NOT Approved = 1;";

		try {

			Statement stmt = con.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery(selectSQL);

			while(rs.next()) {
				Review revTemp = getReviewFromResultSet(rs);
				treeRev.put(revTemp.getTitle(), revTemp);
			}

			return treeRev;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Array Review Vuoto");
        return null;
        
	}

	@Override
	public Review getReviewFromResultSet(ResultSet rs) throws SQLException {
		
		String title = rs.getString("Title");
		String username = rs.getString("Author");
		String text = rs.getString("Testo");
		int vote = rs.getInt("Voto");
		int appr = rs.getInt("Approved");
		int game = rs.getInt("GameId");
		
		return new Review(title, username, text, vote, game, appr);
		
	}

}