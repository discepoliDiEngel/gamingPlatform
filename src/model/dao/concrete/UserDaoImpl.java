package model.dao.concrete;

import java.util.ArrayList;
import java.sql.*;
import java.util.Date;

import database.ConnectionFactory;
import model.Game;
import model.User;
import model.dao.interfaces.*;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class UserDaoImpl implements UserDao {

    private PreparedStatement prStmt;
    private Connection con;

    public UserDaoImpl() {
        con = new ConnectionFactory().getConnection();
    }

    @Override
    public boolean userExist(String email) {
        
        String querySQL = "SELECT email FROM users WHERE email = '"+ email +"';";

		try {

			prStmt = con.prepareStatement(querySQL);
			ResultSet rs = prStmt.executeQuery(querySQL);

			if(rs.next()) {

				rs.close();
				prStmt.close();
				return true;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

        return false;
        
    }

    @Override
    public int login(String email, String password) {

        String loginSQL = "SELECT * FROM users " + "WHERE email = '" + email + "' AND password = '" + password + "' ;";

        try {

            prStmt = con.prepareStatement(loginSQL);

            ResultSet rs = prStmt.executeQuery(loginSQL);

            if (rs.next()) {

                int role = rs.getInt("role");
                rs.close();
                prStmt.close();
                return role;

            } else {

                rs.close();
                prStmt.close();
                return 9;

            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 11;

    }

    @Override
    public boolean register(String username, String email, String name, String surname, String password) {

        String registerUser = "INSERT INTO users (email, name, surname, username, livello, role, experience, password) "
                + "VALUES ('" + email + "', '" + name + "', '" + surname + "', '" + username + "', 0,0,0,'" + password
                + "');";

        try {

            prStmt = con.prepareStatement(registerUser);
            prStmt.executeUpdate(registerUser);

            System.out.println("Registrazione effettuata con successo.");
            prStmt.close();
            return true;
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public User getUser(String email) {

        String SQL = "SELECT * FROM users WHERE email = '" + email + "';";

        try {

            prStmt = con.prepareStatement(SQL);
            ResultSet rs = prStmt.executeQuery(SQL);

            if(rs.next()) {
            	User user = getUserFromResultSet(rs);
                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public ArrayList<User> listUser() {
        
        ArrayList<User> arrayUser = new ArrayList<User>();
		String selectSQL = "SELECT * FROM users WHERE NOT role = 2 ;"; 

		try {

			Statement stmt = con.prepareStatement(selectSQL);
			ResultSet rs = stmt.executeQuery(selectSQL);

			while(rs.next()) {

				User tempUser = getUserFromResultSet(rs);
				arrayUser.add(tempUser);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

        return arrayUser;
        
    }

    @Override
    public boolean editUserInfo(String name, String surname, String vecchiaEmail, String email, String username) {

        String updateSQL = "UPDATE users SET name = '" + name + "', surname = '" + surname + "', " + " username = '"
                + username + "' WHERE email = '" + vecchiaEmail + "';";

        try {

            prStmt = con.prepareStatement(updateSQL);
            prStmt.executeUpdate(updateSQL);

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public boolean editUserPassword(String name, String surname, String email, String username, String password) {

        String updateSQL = "UPDATE users SET name = '" + name + "', surname = '" + surname + "', " + " username = '"
                + username + "' WHERE email = '" + email + "'";

        try {

            prStmt = con.prepareStatement(updateSQL);
            prStmt.executeUpdate(updateSQL);

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean promoteUser(User u) {

        String promoteSQL = "UPDATE users SET role = 1 WHERE email = '" + u.getEmail() + "';";

        try {

            prStmt = con.prepareStatement(promoteSQL);
            prStmt.executeUpdate();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public boolean degradeUser(User u) {

        String promoteSQL = "UPDATE users SET role = 0 WHERE email = '" + u.getEmail() + "';";

        try {

            prStmt = con.prepareStatement(promoteSQL);
            prStmt.executeUpdate();

            return true;

        } catch (SQLException e) {

            e.printStackTrace();

        }

        return false;

    }

    @Override
    public boolean deleteUser(String email) {
        
        String deleteUser = "DELETE FROM users WHERE email = '"+ email +"' ;";

		try {

			prStmt = con.prepareStatement(deleteUser);
			int rs = prStmt.executeUpdate(deleteUser);

			if(rs > 0) {
				//System.out.println("Delete avvenuta con successo.");
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//System.err.println("Errore eliminazione utente!");
        return false;
        
    }

    @Override
    public int getXp(String email) {

        String selectXP = "SELECT experience FROM users WHERE email = '"+ email +"' ;";

		try {

			prStmt = con.prepareStatement(selectXP);
			ResultSet rs = prStmt.executeQuery(selectXP);

			if(rs.next())
				return rs.getInt("experience");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
		
    }

    @Override
    public void editUserXp(String email, int xp) {

        String updateXP = "UPDATE users SET experience = '" + xp + "' WHERE email = '" + email + "' ;";

		try {

			PreparedStatement prStmt = con.prepareStatement(updateXP);
			prStmt.executeUpdate(updateXP);

			System.out.println("Update avvenuta con successo.");

		} catch (SQLException e) {

			e.printStackTrace();
		}
		
		//System.err.print("Errore update punti XP!");

    }

    @Override
    public void updateTimeline(String email, int exp, Game game, Date date) {

        @SuppressWarnings("deprecation")
		String data = date.getDate() + "/" + (date.getMonth()+1) + "/" + (date.getYear()+1900);
		String timelineSQL = "INSERT INTO `timeline` (`GamerId`, `GameId`, `LivelloConquistato`, `Giorno`) VALUES ('" + email + "','" + game.getId() + "','" + exp + "','" + data + "')";

		try {
			
			prStmt = con.prepareStatement(timelineSQL);
			prStmt.executeUpdate(timelineSQL);

			System.out.println("timeline salvata con successo.");
            
		    prStmt.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//System.err.println("Salvataggio timeline fallita.");

    }

    @Override
    public ArrayList<String> getTimeline(String email) {

        ArrayList<String> tempTime = new ArrayList<String>();
        String selectSQL = "SELECT * FROM timeline t INNER JOIN game g ON t.GameId = g.GameId WHERE t.GamerId = '"
                + email + "';";

        try {

            Statement stmt = con.prepareStatement(selectSQL);
            ResultSet rs = stmt.executeQuery(selectSQL);

            while (rs.next()) {

                String nameGame = rs.getString("name");
                String data = rs.getString("Giorno");
                int exp = rs.getInt("LivelloConquistato");

                String timeline = data + " - Hai vinto " + exp + " punti in " + nameGame + ".";
                tempTime.add(timeline);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tempTime;

    }

	@Override
	public User getUserFromResultSet(ResultSet rs) throws SQLException {
		
		String email = rs.getString("email");
        String nome = rs.getString("name");
        String cognome = rs.getString("surname");
        String username = rs.getString("username");
        int exp = rs.getInt("experience");
        int role = rs.getInt("role");
        
        User ut = new User(nome, cognome, email, username, exp, role);
        ut.setTimeline(getTimeline(email));
		return ut;
	}

}