package model.dao.concrete;

import java.sql.Connection;
import database.ConnectionFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Game;
import model.dao.interfaces.GameDao;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class GameDaoImpl implements GameDao {
	
	Game[] games;
	Connection con;
	
	public GameDaoImpl() {
		con = new ConnectionFactory().getConnection();
	}

	@Override
	public Game[] getGames() {

        String gamesSQL = "SELECT * FROM game";
        
        List<Game> lista = new ArrayList<Game>();
		
		try {
			
			PreparedStatement prStmt = con.prepareStatement(gamesSQL);
			ResultSet rs = prStmt.executeQuery();
			
			while(rs.next()) {
				Game g = getGameFromResultSet(rs);
				lista.add(g);
			}
			
			games = lista.toArray(new Game[lista.size()]);
			return games;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        return null; 
        
	}
	
	@Override
	public void updateInfo(int id, String name, String path, float val) {
		
		String updateSQL = "UPDATE game SET name = ?, path = ?, valutazione = ? WHERE id = ?;";
		
		try {
			PreparedStatement prStmt = con.prepareStatement(updateSQL);
			prStmt.setString(1, name);
			prStmt.setString(2, path);
			prStmt.setFloat(3, val);
			prStmt.setInt(4, id);
			
			prStmt.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Game getGameFromResultSet(ResultSet rs) throws SQLException {
		
		int id = rs.getInt("GameId");
		String name = rs.getString("Name");
		String p = rs.getString("Path");
		float m = rs.getFloat("valutazione");
		
		return new Game(name, id, p, m);
		
	}

}