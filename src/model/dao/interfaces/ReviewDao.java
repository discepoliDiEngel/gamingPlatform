package model.dao.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeMap;
import model.Game;
import model.Review;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public interface ReviewDao {
	
	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	public Review getReviewFromResultSet(ResultSet rs) throws SQLException;

    /**
	 * Inserimento recensione di un gioco dall'utente nel database.
	 * 
	 * @param title Titolo recensione <b>(int)</b>
	 * @param author Username dell'utente che ha creato la recensione <b>(String)</b>
	 * @param text Contenuto della Review scritto dall'utente (anche null) <b>(String)</b>
	 * @param vote Voto (da 1 a 10) con cui l'utente ha valutato il gioco <b>(int)</b>
	 * @param gameID ID del gioco recensito <b>(int)</b>
	 */
	public boolean insertReview(String title, String author, String text, int vote, int gameID);
	
	/**
	 * Restituisce le recensioni relative ad un gioco dato in input.
	 * 
	 * @param gameObj Gioco per cui costruire la lista <b>(Game)</b>
	 */
	public TreeMap<String, Review> getReviewsByGame(Game gameObj);

	/**
	 * Approvazione della recensione da parte di un moderatore/amministratore della piattaforma.
	 * 
	 * @param rev Recensione che deve essere approvata <b>(Review)</b>
	 */
	public boolean approveReview(Review rev);

	/**
	 * Eliminazione recensione dell'utente tramite ID gioco e username
	 * 
	 * @param rev Recensione da eliminare <b>(Review)</b>
	 */
	public boolean deleteReview(Review rev);

    /**
	 * Metodo che restitusce un arraylist di tutte le review utilizzate dal controller dell'admin
	 * e dal modeato per approvare o meno le review.
	 * 
	 * @return <b>TreeMap<String, Review></b>
	 */
	public TreeMap<String, Review> mapReview();

}
