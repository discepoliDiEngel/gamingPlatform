package model.dao.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

import model.Game;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public interface GameDao {

    /**
	 * Legge tutti i giochi salvati nel database.
	 * 
	 * @return <b>Game[]</b>
	 */
	public Game[] getGames();
	
	/**
	 * 
	 * @param name
	 * @param path
	 * @param val
	 */
	public void updateInfo(int id, String name, String path, float val);
	
	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	public Game getGameFromResultSet(ResultSet rs) throws SQLException;

}