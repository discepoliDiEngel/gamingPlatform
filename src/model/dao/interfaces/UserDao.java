package model.dao.interfaces;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Game;
import model.User;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public interface UserDao {
	
	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	public User getUserFromResultSet(ResultSet rs) throws SQLException;

    /**
	 * Controlla se l'utente gia' esiste nel database tramite indirizzo email.
	 * 
	 * @param email Email dell'utente <b>(String)</b>
	 * @return <b>boolean</b>
	 */
	public boolean userExist(String email);

	/**
	 * Metodo usato per effetuare il login sulla piattaforma.
	 * 
	 * @param username username dell'utente <b>(String)</b>
	 * @param password password dell'utente <b>(String)</b>
	 * @return <b>boolean</b>
	 */
	int login(String email, String password);

	/**
	 * Registrazione di un nuovo utente.
	 * 
	 * @param username Username utente <b>(String)</b>
	 * @param email Email utente <b>(String)</b>
	 * @param name Nome dell'utente <b>(String)</b>
	 * @param surname Cognome dell'utente <b>(String)</b>
	 * @param password Password account <b>(String)</b>
	 * @return <b>boolean</b>
	 */
	boolean register(String username, String email, String name, String surname, String password);

	/**
	 * Metodo che restituisce un oggetto User.
	 * @param email Indirizzo e-mail dell'utente.  <b>(String)</b>
	 * @return <b>User</b>
	 */
	User getUser(String email);

    /**
	 * Metodo che restituisce un ArrayList di tutti gli utenti registrati sulla piattaforma.
	 * E' utilizzata dall'admin per eliminare, declassare e promuovere utenti.
	 * 
	 * @return <b>ArrayList<User></b>
	 */
	public ArrayList<User> listUser();
	
	/**
	 * Aggiornamento profilo personale utente, restituisce true se l'aggiornamento
	 * e' andata a buon fine.
	 * 
	 * @param name Nome utente <b>(String)</b>
	 * @param surname Cognome utente <b>(String)</b>
	 * @param email Email utente <b>(String)</b>
	 * @param username Username utente <b>(String)</b>
	 */
	boolean editUserInfo(String name, String surname, String vecchiaEmail, String email, String username);
	
	/**
	 * Aggiornamento password utente.
	 * 
	 * @param name Nome utente <b>(String)</b>
	 * @param surname Cognome utente <b>(String)</b>
	 * @param email Email utente <b>(String)</b>
	 * @param username Username utente <b>(String)</b>
	 * @param password Nuova password <b>(String)</b>
	 * @return <b>boolean</b>
	 */
	boolean editUserPassword(String name, String surname, String email, String username, String password);

	/**
	 * Promuove un utente a moderatore.
	 * 
	 * @param u Utente da promuovere. <b>(User)</b>
	 * @return <b>boolean</b>
	 */
	boolean promoteUser(User u);
	
	/**
	 * Degrada un moderatore a utente base.
	 * 
	 * @param u Utente da degradare <b>(User)</b>
	 * @return <b>boolean</b>
	 */
	boolean degradeUser(User u);

	/**
	 * Eliminazione utente utente dalla piattaforma tramite username.
	 * 
	 * @param email Email dell'utente da eliminare <b>(String)</b>
	 */
	boolean deleteUser(String email);

	/**
	 * Restituisce i punti esperienza dell'utente tramite username.
	 * 
	 * @param username Username utente <b>(String)</b>
	 * @return <b>int</b>
	 */
	public int getXp(String email);


	/**
	 * Aggiornamento XP totali user nel database.
	 * 
	 * @param email E-mail dell'utente per aggiornare i punti esperienza <b>(String)</b>
	 * @param xp XP totali dell'utente <b>(int)</b>
	 */
	public void editUserXp(String email, int xp);

	/**
	 * Aggiornamento timeline utente per il raggiungimento di un nuovo livello utente.
	 * 
	 * @param email E-mail utente. <b>(String)</b>
	 * @param exp Esperienza acquisita <b>(int)</b>
	 * @param game Gioco per cui e' stato raggiunto un nuovo livello. <b>(Game)</b>
	 * @param date Data in cui e' avvenuto l'evento. <b>(Date)</b>
	 */
	public void updateTimeline(String email, int exp, Game game, Date date);

	/**
	 * Legge la timeline di un utente.
	 * 
	 * @param email E-mail utente. <b>(String)</b>
	 * @return <b>ArrayList<String></b>
	 */
	public ArrayList<String> getTimeline(String email);
    
}