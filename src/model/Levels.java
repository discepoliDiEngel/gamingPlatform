package model;

/**
 * Interfaccia usata per impostare i punti esperienza necessari alla raggiunta di un livello.
 * 
 * @author Discepoli di Engel
 *
 */
public interface Levels {
	
	public static final int LIVELLO_0 = 0;
	public static final int LIVELLO_1 = 100;
	public static final int LIVELLO_2 = 200;
	public static final int LIVELLO_3 = 300;
	public static final int LIVELLO_4 = 400;
	public static final int LIVELLO_5 = 500;
	public static final int LIVELLO_6 = 600;
	public static final int LIVELLO_7 = 700;
	public static final int LIVELLO_8 = 800;
	public static final int LIVELLO_9 = 900;
	public static final int LIVELLO_10 = 1000;
	
}
