package model;

/**
 * Classe usata per rappresentare le recensioni che vengono scritte dagli utenti.
 * 
 * @author Discepoli di Engel
 *
 */
public class Review {
	
	private String title;
	private String text;
	private int vote;
	private boolean approved;
	private String username;
	private int game;

	/**
	 * Costruttore Completo.
	 * 
	 * @param text Testo recensione.
	 * @param vote Voto recensione.
	 * @param username Username autore.
	 */
	public Review(String title, String username, String text, int vote, int approved, int game) {
		this.title = title;
		this.username = username;
		this.text = text;
		this.vote = vote;
		if(approved > 0) this.approved = true;
		else this.approved = false;
	}

	/**
	 * Costruttore usato quando viene scritta una nuova recensione, che verra' poi salvata nel database.
	 * 
	 * @param title Titolo recensione.
	 * @param username Username autore.
	 * @param text Testo recensione.
	 * @param vote Voto recensione.
	 * @param game Gioco recensione.
	 */
	public Review(String title, String username, String text, int vote, int game) {
		this.title = title;
		this.username = username;
		this.text = text;
		this.vote = vote;
		this.game = game;
		this.approved = false;
	}

	/**
	 * Restituisce il titolo della Review.
	 * 
	 * @return String
	 */
	public String getTitle() { return title; }

	/**
	 * Restituisce il testo della Review.
	 * 
	 * @return String
	 */
	public String getText() { return text; }

	/**
	 * Restituisce il voto della Review.
	 * 
	 * @return int
	 */
	public int getVote() { return vote; }

	/**
	 * Restituisce lo stato dell'approvazione della Review.
	 * 
	 * @return boolean
	 */
	public boolean getApproved() { 
		return approved;
	}
	/**
	 * Restituisce l'username della Review.
	 * 
	 * @return String
	 */
	public String getUsername() { 
		return username; 
	}
	
	/**
	 * Restituisce l'id del gioco a cui e' associata la Review.
	 * @return int
	 */
	public int getGame() {
		return game;
	}

	/**
	 * Assegna un testo alla recensione.
	 * 
	 * @param t testo da assegnare
	 */
	public void setText(String t) { text = t; }

	/**
	 * Assegna un voto alla recensione.
	 * 
	 * @param v voto 
	 */
	public void setVote(int v) { vote = v; }

	/**
	 * Assegna stato alla recensione.
	 * 
	 * @param approved Valore booleano
	 */
	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	/**
	 * Compara due oggetti, restituisce vero se uguali, false altrimenti.
	 * 
	 * @return boolean
	 */
	public boolean equals (Object obj) {
		
		if (obj == null) return false;
		if (!(obj instanceof Review)) return false;
		
		Review a = (Review) obj;
		
		return this.getText().equals(a.getText())&&
				this.getVote() == a.getVote() &&
				this.getGame() == a.getGame() &&
				this.getUsername().equals(a.getUsername());

	}

	/**
	 * Restituisce una stringa con tutte le informazioni relative a questo oggetto.
	 * 
	 * @return String
	 */
	public String toString () {
		return " Text: " + getText() + " Vote: " + getVote() + " Username: " 
				+ getUsername() + " Game ID: " + getGame();
	}

}


