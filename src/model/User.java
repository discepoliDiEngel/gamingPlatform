package model;

import java.util.ArrayList;

/**
 * Classe che rappresenta tutti gli utenti.
 * 
 * @author Discepoli di Engel
 *
 */
public class User implements Levels{
	
	private String nome;
	private String cognome;
	private String email;
	private String username;
	private String role;
	private int experience;
	private ArrayList<String> timeline;

	/**
	 * Costruttore Completo.
	 * 
	 * @param nome Nome utente
	 * @param cognome Cognome utente
	 * @param email e-mail utente
	 * @param username Username utente
	 */
	public User(String nome, String cognome, String email, String username, int exp, int role) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.setEmail(email);
		this.setUsername(username);
		this.setExperience(exp);
		
		switch(role) {
		
		case 0: 
			this.setRole("Gamer");
			break;
			
		case 1:
			this.setRole("Moderator");
			break;
			
		case 2:
			this.setRole("Administrator");
			break;
			
		}
	
	}
	
	/**
	 * Calcola il livello dell'utente in base ai livelli impostati innell'interfaccia Levels
	 * @return
	 */
	public int calcLevel() {
		if(experience < LIVELLO_1)
			return 0;		
		if(experience < LIVELLO_2)
			return 1;
		if(experience < LIVELLO_3)
			return 2;
		if(experience < LIVELLO_4)
			return 3;
		if(experience < LIVELLO_5)
			return 4;
		if(experience < LIVELLO_6)
			return 5;
		if(experience < LIVELLO_7)
			return 6;
		if(experience < LIVELLO_8)
			return 7;
		if(experience < LIVELLO_9)
			return 8;
		if(experience < LIVELLO_10)
			return 9;
		else return 10;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the timeline
	 */
	public ArrayList<String> getTimeline() {
		return timeline;
	}

	/**
	 * @param timeline the timeline to set
	 */
	public void setTimeline(ArrayList<String> timeline) {
		this.timeline = timeline;
	}


	/**
	 * @return the experience
	 */
	public int getExperience() {
		return experience;
	}


	/**
	 * @param experience the experience to set
	 */
	public void setExperience(int experience) {
		this.experience = experience;
	}

}