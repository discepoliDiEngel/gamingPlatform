package model;

import java.io.File;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class Game {
	
	private String nome;
	private String path;
	private int id;
	private float mediaReview;

	/**
	 * Costruttore base.
	 */
	public Game() {}
	
	/**
	 * Costruttore Base.
	 * 
	 * @param name Nome gioco
	 * @param path Percorso dove risiede il gioco.
	 * @param media Media dei voti delle recensioni.
	 * @param lista Lista delle recensioni.
	 */
	public Game(String name, int id, String path, float media) {
		nome = name;
		this.path = path;
		this.id = id;
		mediaReview = media;
	}
	
	/**
	 * Calcola il path del gioco.
	 * 
	 * @return String
	 */
	public String findPath() {
		
		File file = new File(this.getName() + ".jar");
		String dir = file.getAbsoluteFile().getParent();
		
		setPath(dir.concat("\\games\\")); 
		return path;
		
	}

	/**
	 * Ogni volta che un utente lascia una valutazione del gioco viene richiamato dal costruttore 
	 * di Review e aggiorna la media del gioco.
	 * 
	 * @param valutazione valutazione e' un valore float 
	 */
	public void calcolaMedia(float valutazione) {
		this.mediaReview = (mediaReview + valutazione) / 2;
	}
	
	/**
	 * Restituisce il nome del gioco.
	 * 
	 * @return String
	 */
	public String getName() { return nome; }
	
	/**
	 * Restituisce il path della directory dove il gioco e' stato salvato.
	 * 
	 * @return String
	 */
	public String getPath() { return path; }
	
	/**
	 * Restituisce la media delle valutazioni relative al gioco.
	 * 
	 * @return float
	 */
	public float getMediaReview() { return mediaReview; }
	
	/**
	 * 
	 * @return int
	 */
	public int getId() { return id; }
	
	/**
	 * Imposta il nome del gioco.
	 * 
	 * @param name Nuovo nome.
	 */
	public void setName(String name) { this.nome = name; }
	
	/**
	 * Imposta il path dellla directory dove il gioco e' salvato.
	 * 
	 * @param path Percorso gioco.
	 */
	public void setPath(String path) { 
		this.path = path;		
	}
	
}
