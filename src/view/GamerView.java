package view;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import model.Game;
import model.User;
import view.controller.GamerController;
import view.renderer.*;

/**
 * Finestra con cui interagiscono gli utenti che possono usufruire del servizio
 * di gioco.
 * 
 * @author Discepoli di Engel
 *
 */
public class GamerView {

	GamerController controller;
	boolean svuotaReview = false;
	boolean svuotaCommenta = false;
	boolean svuotaTitolo = false;

	private JFrame frame;
	
	/*
	 * Inizializzazione degli oggetti contenuti nel frame.
	 */
	JLabel lblCenter = new JLabel("New label");
	JLabel nomeGiocatore = new JLabel("New label");
	JLabel lblGioco = new JLabel("New label");
	JLabel lblGioco2 = new JLabel("New label");
	JLabel lblGioco0 = new JLabel("New label");
	JLabel lblGioco3 = new JLabel("New label");
	JLabel lblRight = new JLabel("New label");
	JLabel lblMessage = new JLabel("New label");
	JLabel lblPunteggio = new JLabel("Punteggio");
	JLabel lblStar01 = new JLabel("New label");
	JLabel lblStar02 = new JLabel("New label");
	JLabel lblStar03 = new JLabel("New label");
	JLabel lblStar04 = new JLabel("New label");
	JLabel lblStar05 = new JLabel("New label");
	JButton btnGioca = new JButton("Gioca");
	JButton btnRecensisci = new JButton("Recensisci");
	JButton btnAnnulla = new JButton("Annulla");
	JList<String> commentList = new JList<String>();
	JTextArea lblCommento = new JTextArea("Commento");
	JTextArea lblDescrizione = new JTextArea("Descrizione");
	JTextField txtReview;
	JTextField txtCommenta;
	JTextField txtTitolo;

	/*
	 * Inizializzazione delle stelle per il punteggio.
	 */
	String star0 = "resources/img/profileView/starYellow.png";
	String star1 = "resources/img/profileView/starYellow.png";
	String star2 = "resources/img/profileView/starYellow.png";
	String star3 = "resources/img/profileView/starYellow.png";
	String star4 = "resources/img/profileView/starYellow.png";

	/**
	 * Costruttore della view dell'utente base.
	 * 
	 * @param contr Controller utente giocatore
	 * @param gamer Utente gamer
	 * @param exp Esperienza utente
	 * @param lev Livello utente
	 * @param games Giochi offerti dalla piattaforma
	 * @param tipoGamer Tipo utente
	 */
	public GamerView(User user) {
		
		controller = new GamerController(user, this);
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize(user, controller.getGames());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Inizializza la view del gamer
	 * @param gamer
	 * @param controller
	 * @param exp
	 * @param lev
	 * @param games
	 * @param tipoGamer
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private void initialize(User gamer, Game[] games) {

		/*
		 * Immagini e icone
		 */
		ImageIcon backgroundAdmin = new ImageIcon("resources/img/wallpaper/wp04.jpg");
		ImageIcon topBar = new ImageIcon("resources/img/admin/TitleBar.png");
		ImageIcon footerBar = new ImageIcon("resources/img/admin/FooterBar.png");
		ImageIcon logo = new ImageIcon("resources/img/admin/boltLogo.png");
		ImageIcon user = new ImageIcon("resources/img/admin/userImage.png");
		ImageIcon logout = new ImageIcon("resources/img/admin/exitButton.png");
		ImageIcon topBarBottom = new ImageIcon("resources/img/admin/TitleBarBottom.png");
		ImageIcon gameArt = new ImageIcon("resources/img/secondView/gameArt01.png");
		ImageIcon gameArt0 = new ImageIcon("resources/img/secondView/Breakout.png");
		ImageIcon gameArt3 = new ImageIcon("resources/img/secondView/Tetris.png");
		ImageIcon gameArt2 = new ImageIcon("resources/img/secondView/SpaceInvaders.png");
		ImageIcon gameListImage = new ImageIcon("resources/img/admin/listSectionAdmin.png");
		ImageIcon gameListImageOmbra = new ImageIcon("resources/img/secondView/Ombra.png");
		ImageIcon messageBar = new ImageIcon("resources/img/secondView/messageBar.png");

		/*
		 * Stelle giocatore
		 */
		ImageIcon varstar0 = new ImageIcon(star0);
		ImageIcon varstar1 = new ImageIcon(star1);
		ImageIcon varstar2 = new ImageIcon(star2);
		ImageIcon varstar3 = new ImageIcon(star3);
		ImageIcon varstar4 = new ImageIcon(star4);

		/*
		 * Tutto il Frame.
		 */
		frame = new JFrame();
		frame.setTitle("BOLT Gaming Platform - Let's Play");
		frame.setBounds(100, 0, 1280, 820);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);

		/*
		 * Al click del Mouse sul frame chiude tutto lasciando aperta soltanto la lista dei giochi.
		 */
		frame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				commentList.setVisible(false);
				txtReview.setVisible(false);
				txtTitolo.setVisible(false);
				txtCommenta.setVisible(false);
				lblRight.setVisible(false);
				lblGioco.setVisible(false);
				lblGioco0.setVisible(false);
				lblGioco2.setVisible(false);
				lblGioco3.setVisible(false);
				lblCenter.setVisible(false);
				btnGioca.setVisible(false);
				btnRecensisci.setVisible(false);
				btnAnnulla.setVisible(false);
				lblMessage.setVisible(false);
				lblCommento.setVisible(false);
				lblCommento.setOpaque(false);
				lblDescrizione.setVisible(false);
				lblPunteggio.setVisible(false);
				lblStar01.setVisible(false);
				lblStar02.setVisible(false);
				lblStar03.setVisible(false);
				lblStar04.setVisible(false);
				lblStar05.setVisible(false);
				lblDescrizione.setVisible(false);
				frame.validate();
				frame.repaint();

			}
		});

		/*
		 * Lista dei giochi sinistra.
		 */
		JList<Game> gameList = new JList<Game>(games);
		gameList.setCellRenderer(new GameCellRenderer());

		/*
		 * Al Click del Mouse rende visibile la parte Centrale con tutti gli elementi.
		 */
		gameList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Game game = gameList.getSelectedValue();
				cambiaStella(game.getMediaReview());

				switch (game.getId()) {
				
				case 0:
					lblGioco.setVisible(false);
					lblGioco0.setVisible(true);
					lblGioco2.setVisible(false);
					lblGioco3.setVisible(false);
					break;
					
				case 1:
					lblGioco.setVisible(true);
					lblGioco0.setVisible(false);
					lblGioco2.setVisible(false);
					lblGioco3.setVisible(false);
					break;
					
				case 2:
					lblGioco.setVisible(false);
					lblGioco0.setVisible(false);
					lblGioco2.setVisible(true);
					lblGioco3.setVisible(false);
					break;
					
				case 3:
					lblGioco.setVisible(false);
					lblGioco0.setVisible(false);
					lblGioco2.setVisible(false);
					lblGioco3.setVisible(true);
					break;
					
				}

				lblCenter.setVisible(true);
				btnGioca.setVisible(true);
				lblDescrizione.setVisible(true);
				lblPunteggio.setVisible(true);
				lblMessage.setVisible(true);
				lblRight.setVisible(true);
				commentList.setVisible(true);
				txtReview.setVisible(false);
				txtTitolo.setVisible(false);
				txtCommenta.setVisible(false);
				btnRecensisci.setVisible(false);
				btnAnnulla.setVisible(false);

				// TODO: Prendere oggetto game selezionato e richiedere le recensioni al controller
				commentList.setListData( controller.getReviewsTitles( controller.getReviews(gameList.getSelectedValue()) ) );
				lblPunteggio.setText(String.valueOf(game.getMediaReview()));

				frame.validate();
				frame.repaint();

			}
		});
		gameList.setBorder(null);
		gameList.setSelectionBackground(new Color(2, 119, 189));
		gameList.setOpaque(false);
		frame.getContentPane().setLayout(null);
		gameList.setBounds(40, 100, 400, 600);
		frame.getContentPane().add(gameList);
		gameList.setFont(new Font("Menlo", Font.PLAIN, 14));
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setVisible(false);
		scrollPane1.setOpaque(false);
		scrollPane1.setPreferredSize(new Dimension(500, 100));
		scrollPane1.setLocation(40, 100);
		scrollPane1.setBounds(40, 100, 400, 600);
		scrollPane1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		frame.getContentPane().add(scrollPane1);
		frame.setResizable(false);

		/*
		 * Stelle punteggio.
		 */
		lblStar01 = new JLabel(varstar0);
		lblStar01.setBorder(null);
		lblStar01.setBounds(460, 240, 30, 30);
		lblStar01.setVisible(false);
		frame.getContentPane().add(lblStar01);

		lblStar02 = new JLabel(varstar1);
		lblStar02.setBorder(null);
		lblStar02.setBounds(500, 240, 30, 30);
		lblStar02.setVisible(false);
		frame.getContentPane().add(lblStar02);

		lblStar03 = new JLabel(varstar2);
		lblStar03.setBorder(null);
		lblStar03.setBounds(540, 240, 30, 30);
		lblStar03.setVisible(false);
		frame.getContentPane().add(lblStar03);

		lblStar04 = new JLabel(varstar3);
		lblStar04.setBorder(null);
		lblStar04.setBounds(580, 240, 30, 30);
		lblStar04.setVisible(false);
		frame.getContentPane().add(lblStar04);

		lblStar05 = new JLabel(varstar4);
		lblStar05.setBorder(null);
		lblStar05.setBounds(620, 240, 30, 30);
		lblStar05.setVisible(false);
		frame.getContentPane().add(lblStar05);

		/*
		 * Label che descrive il Gioco.
		 */
		lblDescrizione = new JTextArea();
		lblDescrizione.setFont(new Font("Menlo", Font.PLAIN, 14));
		lblDescrizione.setLineWrap(true);
		lblDescrizione.setForeground(Color.WHITE);
		lblDescrizione.setBorder(null);
		lblDescrizione.setBounds(450, 310, 380, 380);
		lblDescrizione.setBackground(new Color(236, 240, 241));
		lblDescrizione.setOpaque(false);
		lblDescrizione.setVisible(false);
		frame.getContentPane().add(lblDescrizione);
		lblDescrizione.setText(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

		/*
		 * Label Punteggio.
		 */
		lblPunteggio = new JLabel();
		lblPunteggio.setFont(new Font("Menlo", Font.PLAIN, 48));
		lblPunteggio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPunteggio.setForeground(Color.WHITE);
		lblPunteggio.setBorder(null);
		lblPunteggio.setBounds(630, 220, 200, 80);
		lblPunteggio.setBackground(new Color(236, 240, 241));
		lblPunteggio.setOpaque(false);
		lblPunteggio.setVisible(false);
		frame.getContentPane().add(lblPunteggio);

		
		
		/*
		 * Field dove viene inserito il Corpo del commento.
		 */
		txtReview = new JTextField();
		txtReview.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtReview.setForeground(Color.BLACK);
		txtReview.setBackground(new Color(224, 224, 224, 150));
		txtReview.setBounds(840, 580, 400, 40);
		txtReview.setBorder(null);
		txtReview.setEnabled(true);
		txtReview.setVisible(false);
		txtReview.setBorder(BorderFactory.createCompoundBorder(txtReview.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtReview);
		txtReview.setText("Esprimi un voto da 1 a 5");
		
		/*
		 * Field dove viene inserito il titolo del commento.
		 */
		txtTitolo = new JTextField();
		txtTitolo.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtTitolo.setForeground(Color.BLACK);
		txtTitolo.setBackground(new Color(224, 224, 224, 150));
		txtTitolo.setBounds(840, 620, 400, 40);
		txtTitolo.setBorder(null);
		txtTitolo.setEnabled(true);
		txtTitolo.setVisible(false);
		txtTitolo.setBorder(
				BorderFactory.createCompoundBorder(txtTitolo.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtTitolo);
		txtTitolo.setText("Inserisci qui il titolo della Review");

		/*
		 * Field dove viene inserito il Corpo del commento.
		 */
		txtCommenta = new JTextField();
		txtCommenta.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtCommenta.setForeground(Color.BLACK);
		txtCommenta.setBackground(new Color(224, 224, 224, 150));
		txtCommenta.setBounds(840, 660, 400, 40);
		txtCommenta.setBorder(null);
		txtCommenta.setEnabled(true);
		txtCommenta.setVisible(false);
		txtCommenta.setBorder(BorderFactory.createCompoundBorder(txtCommenta.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtCommenta);
		txtCommenta.setText("Inserisci qui la tua Review");

		/*
		 * Lista dei commenti, Destra.
		 */

		/*
		 * Label dove viene visualizzato il Commento una volta cliccato.
		 */
		lblCommento = new JTextArea();
		lblCommento.setBorder(null);
		lblCommento.setLineWrap(true);
		lblCommento.setBounds(840, 420, 400, 280);
		lblCommento.setBackground(new Color(236, 240, 241));
		lblCommento.setEditable(false);
		lblCommento.setOpaque(false);
		lblCommento.setVisible(false);
		frame.getContentPane().add(lblCommento);

		/*
		 * Al click del mouse su un elemento della lista vengono visualizzati gli elementi all'interno del metodo.
		 */
		commentList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO: Inserire commenti
				lblCommento.setText(controller.getReviews(gameList.getSelectedValue()).get((String) commentList.getSelectedValue()).getText());
				lblCommento.setVisible(true);
				lblCommento.setOpaque(true);
			}
		});
		commentList.setVisible(false);
		commentList.setBorder(null);
		commentList.setSelectionBackground(new Color(231, 76, 60));
		commentList.setOpaque(false);
		frame.getContentPane().setLayout(null);
		commentList.setBounds(840, 140, 400, 560);
		frame.getContentPane().add(commentList);
		commentList.setFont(new Font("Menlo", Font.PLAIN, 14));
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setVisible(false);
		scrollPane2.setOpaque(false);
		scrollPane2.setPreferredSize(new Dimension(500, 100)); //Ridimensiona il JScrollPane
		scrollPane2.setLocation(40, 100);
		scrollPane2.setBounds(40, 100, 400, 600);
		frame.getContentPane().add(scrollPane2);
		scrollPane2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		frame.setResizable(false);

		/*
		 * Label che mostra la copertina del gioco. Viene visualizzata al click sul nome del gioco.
		 */
		lblGioco = new JLabel(gameArt);
		lblGioco.setBorder(null);
		lblGioco.setVisible(false);
		lblGioco.setBounds(440, 100, 400, 120);
		frame.getContentPane().add(lblGioco);

		lblGioco0 = new JLabel(gameArt0);
		lblGioco0.setBorder(null);
		lblGioco0.setVisible(false);
		lblGioco0.setBounds(440, 100, 400, 120);
		frame.getContentPane().add(lblGioco0);

		lblGioco2 = new JLabel(gameArt2);
		lblGioco2.setBorder(null);
		lblGioco2.setVisible(false);
		lblGioco2.setBounds(440, 100, 400, 120);
		frame.getContentPane().add(lblGioco2);

		lblGioco3 = new JLabel(gameArt3);
		lblGioco3.setBorder(null);
		lblGioco3.setVisible(false);
		lblGioco3.setBounds(440, 100, 400, 120);
		frame.getContentPane().add(lblGioco3);

		/*
		 * Elemento ombra sulla parte centrale.
		 */
		lblCenter = new JLabel(gameListImageOmbra);
		lblCenter.setBorder(null);
		lblCenter.setVisible(false);
		lblCenter.setBounds(440, 100, 400, 600);
		frame.getContentPane().add(lblCenter);

		/*
		 * Message bar, elemento decorativo di colore blu posizionato sopra la lista di destra, lista commenti.
		 */
		lblMessage = new JLabel(messageBar);
		lblMessage.setBorder(null);
		lblMessage.setVisible(false);
		lblMessage.setBounds(840, 100, 400, 40);
		frame.getContentPane().add(lblMessage);

		/*
		 * Pulsante Recensisci, al click prende in input Titolo e Corpo e li pubblica in attesa di una moderazione.
		 */
		btnRecensisci.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String review = txtReview.getText();
				String title = txtTitolo.getText();
				String corpo = txtCommenta.getText();
				System.out.println("Review" + review);
				System.out.println("Titolo " + title);
				System.out.println("Corpo " + corpo);
				
				// TODO: Da implementare la review.
				controller.writeReview(title, gamer.getEmail(), corpo, 0, gameList.getSelectedValue().getId());
				JOptionPane.showMessageDialog(null, "Complimenti " + gamer.getUsername() + " hai recensito il gioco!");

			}
		});
		btnRecensisci.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnRecensisci.setForeground(Color.WHITE);
		btnRecensisci.setBounds(840, 700, 200, 40);
		frame.getContentPane().add(btnRecensisci);
		btnRecensisci.setBorderPainted(false);
		btnRecensisci.setBackground(new Color(139, 195, 74));
		btnRecensisci.setOpaque(true);
		btnRecensisci.setVisible(false);

		/*
		 * Pulsante Annulla, al click svuota le Text Titolo e Corpo.
		 */
		btnAnnulla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtReview.setText(null);
				txtCommenta.setText(null);
				txtTitolo.setText(null);
				txtReview.setText("Esprimi un voto da 1 a 5");
				txtTitolo.setText("Inserisci qui il titolo della Review");
				txtCommenta.setText("Inserisci qui la tua Review");
				System.out.println("Commento Eliminato");
				txtCommenta.validate();
				txtCommenta.repaint();
				txtTitolo.validate();
				txtTitolo.repaint();
				JOptionPane.showMessageDialog(null,
						"Hey " + gamer.getUsername() + " questo gioco non ti e' piaciuto? Lascia una Review :-)");
			}
		});
		btnAnnulla.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnAnnulla.setForeground(Color.WHITE);
		btnAnnulla.setBounds(1040, 700, 200, 40);
		frame.getContentPane().add(btnAnnulla);
		btnAnnulla.setBorderPainted(false);
		btnAnnulla.setBackground(new Color(255, 87, 34));
		btnAnnulla.setOpaque(true);
		btnAnnulla.setVisible(false);

		/*
		 * Pulsante Gioca, al click mostra la Sezione di Sinistra e tutti gli elementi all'interno del metodo.
		 */
		btnGioca.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				lblRight.setVisible(true);
				txtReview.setVisible(true);
				txtCommenta.setVisible(true);
				txtTitolo.setVisible(true);
				commentList.setVisible(true);
				btnRecensisci.setVisible(true);
				btnAnnulla.setVisible(true);
				lblMessage.setVisible(true);

				controller.play(gameList.getSelectedValue());

				frame.validate();
				frame.repaint();
			}

		});
		btnGioca.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnGioca.setForeground(Color.WHITE);
		btnGioca.setBounds(440, 700, 400, 40);
		frame.getContentPane().add(btnGioca);
		btnGioca.setBorderPainted(false);
		btnGioca.setBackground(new Color(255, 193, 7));
		btnGioca.setOpaque(true);
		btnGioca.setVisible(false);

		/*
		 * Label Gamelist.
		 */
		JLabel lblGameList = new JLabel("Game List");
		lblGameList.setForeground(Color.WHITE);
		lblGameList.setHorizontalAlignment(SwingConstants.CENTER);
		lblGameList.setBorder(null);
		lblGameList.setBounds(40, 74, 400, 26);
		frame.getContentPane().add(lblGameList);

		/*
		 * Immagine sezione sinistra.
		 */
		JLabel lblLeft = new JLabel("New label");
		lblLeft = new JLabel(gameListImage);
		lblLeft.setBorder(null);
		lblLeft.setBounds(40, 100, 400, 600);
		frame.getContentPane().add(lblLeft);

		/*
		 * Immagine sezione Destra
		 */
		lblRight = new JLabel(gameListImageOmbra);
		lblRight.setBorder(null);
		lblRight.setBounds(840, 100, 400, 600);
		lblRight.setVisible(false);
		frame.getContentPane().add(lblRight);

		/*
		 *  Nome giocatore
		 */
		nomeGiocatore.setBorder(null);
		nomeGiocatore.setFont(new Font("Menlo", Font.PLAIN, 12));
		nomeGiocatore.setForeground(Color.WHITE);
		nomeGiocatore.setHorizontalAlignment(SwingConstants.RIGHT);
		nomeGiocatore.setBounds(1010, 10, 150, 40);
		nomeGiocatore.setOpaque(false);
		nomeGiocatore.setVisible(true);
		nomeGiocatore.setText("<html>Benvenuto" + "<br>" + gamer.getUsername() + "!</html>");
		frame.getContentPane().add(nomeGiocatore);

		/*
		 * Image User
		 */
		JButton lblUserImage = new JButton("New label");
		lblUserImage = new JButton(user);
		lblUserImage.setBorder(null);
		lblUserImage.setBounds(1170, 10, 40, 40);
		frame.getContentPane().add(lblUserImage);

		/*
		 * Pulsante Logout
		 */
		JButton btnLogout = new JButton("");
		btnLogout = new JButton(logout);
		btnLogout.setBorder(null);
		btnLogout.setBounds(1230, 10, 40, 40);
		frame.getContentPane().add(btnLogout);

		/*
		 * Logo Bolt
		 */
		JLabel lblLogo = new JLabel("New label");
		lblLogo = new JLabel(logo);
		lblLogo.setBorder(null);
		lblLogo.setBounds(580, 0, 120, 58);
		frame.getContentPane().add(lblLogo);

		/*
		 * Top Bar
		 */
		JLabel lblTopBar = new JLabel("New label");
		lblTopBar = new JLabel(topBar);
		lblTopBar.setBorder(null);
		lblTopBar.setBounds(0, 0, 1280, 60);
		frame.getContentPane().add(lblTopBar);

		/*
		 * Top Bar 2
		 */
		JLabel lblTopBarBottom = new JLabel("New label");
		lblTopBarBottom = new JLabel(topBarBottom);
		lblTopBarBottom.setBorder(null);
		lblTopBarBottom.setBounds(0, 60, 1280, 4);
		frame.getContentPane().add(lblTopBarBottom);

		/*
		 * Footer Bar
		 */
		JLabel lblFooterBar = new JLabel("Copyright @2017 - BOLT");
		lblFooterBar = new JLabel(footerBar);
		lblFooterBar.setForeground(Color.WHITE);
		lblFooterBar.setHorizontalTextPosition(SwingConstants.CENTER);
		lblFooterBar.setText("Copyright 2017 - BOLT");
		lblFooterBar.setBorder(null);
		lblFooterBar.setBounds(0, 760, 1280, 40);
		frame.getContentPane().add(lblFooterBar);

		/*
		 * Immagine sfondo
		 */
		JLabel lblSfondo = new JLabel("New label");
		lblSfondo = new JLabel(backgroundAdmin);
		lblSfondo.setBorder(null);
		lblSfondo.setBounds(0, 0, 1280, 800);
		frame.getContentPane().add(lblSfondo);

		/*
		 * Logout
		 */
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				frame.dispose();
				new FirstView();
			}
		});

		/*
		 * Label User Image
		 */
		lblUserImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ProfileView(gamer, gamer.getRole());
			}
		});

		/*
		 * Svuota il campo Review.
		 */
		txtReview.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaReview == false)
					txtReview.setText("");
				svuotaReview = true;
			}
		});
		
		/*
		 * Svuota il campo Commenta.
		 */
		txtCommenta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaCommenta == false)
					txtCommenta.setText("");
				svuotaCommenta = true;
			}
		});

		/*
		 * Svuota il campo Titolo.
		 */
		txtTitolo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaTitolo == false)
					txtTitolo.setText("");
				svuotaTitolo = true;
			}
		});

	}

	/**
	 * Cambia il numero di stelle in base al livello.
	 * 
	 * @param punteggio Punti esperienza guadagnati dall'utente.
	 */
	public void cambiaStella(float punteggio) {

		if (punteggio <= 1) {

		} else if ((punteggio > 1) && (punteggio < 2)) {
			lblStar01.setVisible(true);
			lblStar02.setVisible(false);
			lblStar03.setVisible(false);
			lblStar04.setVisible(false);
			lblStar05.setVisible(false);
		} else if ((punteggio >= 2) && (punteggio < 3)) {
			lblStar01.setVisible(true);
			lblStar02.setVisible(true);
			lblStar03.setVisible(false);
			lblStar04.setVisible(false);
			lblStar05.setVisible(false);
		} else if ((punteggio >= 3) && (punteggio < 4)) {
			lblStar01.setVisible(true);
			lblStar02.setVisible(true);
			lblStar03.setVisible(true);
			lblStar04.setVisible(false);
			lblStar05.setVisible(false);
		} else if ((punteggio >= 4) && (punteggio < 5)) {
			lblStar01.setVisible(true);
			lblStar02.setVisible(true);
			lblStar03.setVisible(true);
			lblStar04.setVisible(true);
			lblStar05.setVisible(false);
		} else if (punteggio >= 5) {
			lblStar01.setVisible(true);
			lblStar02.setVisible(true);
			lblStar03.setVisible(true);
			lblStar04.setVisible(true);
			lblStar05.setVisible(true);
		}

	}

}