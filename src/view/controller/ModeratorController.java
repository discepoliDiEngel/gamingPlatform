package view.controller;

import java.util.TreeMap;

import controller.GameController;
import controller.ReviewController;
import model.*;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class ModeratorController {

	ReviewController reviewController = new ReviewController();
	GameController gameController = new GameController();

	/**
	 * Costruttore basico
	 */
	public ModeratorController() {

	}

	/**
	 * Carica le recensioni per un determinato gioco.
	 * 
	 * @param game Gioco per cui vengono caricate le recensioni.
	 */
	public TreeMap<String, Review> visualizzaRecensioni(Game game) {
		return reviewController.visualizzaRecensioni(game);
	}

	/**
	 * Approva una recensione e lo comunica al Db. 
	 * 
	 * @param rec Review
	 */
	public void approvaRecensione(Review rec) {
		reviewController.approvaRecensione(rec);
	}

	/**
	 * Elimina una recensione dal database.
	 * 
	 * @param review
	 */
	public void eliminaReview(Review review) {
		reviewController.eliminaReview(review);
	}

	/**
	 * @return the arrayTitle
	 */
	public String[] getArrayTitle() {
		return reviewController.getTitles();
	}

	/**
	 * @param array to set to arrayTitle
	 */
	public void setArrayTitle(String[] array) {
		reviewController.setTitles(array);;
	}

	/**
	 * @return the reviewTree
	 */
	public TreeMap<String, Review> getReviewTree() {
		return reviewController.getReviewTree();
	}

	/**
	 * @param map to set to reviewTree
	 */
	public void setReviewTree(TreeMap<String, Review> map) {
		reviewController.setReviewTree(map);
	}

}
