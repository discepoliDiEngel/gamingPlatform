package view.controller;

import controller.UserController;
import model.User;
import view.*;

/**
 * Prima finestra di avvio del programma. Tramite questa finestra di puo' effettuare la registrazione 
 * sulla piattaforma oppure il login.
 */
public class FirstViewController {

	FirstView fView;
	UserController userController = new UserController();
	
	/**
	 * Costruisce un nuovo controller in cui la connessione con il database manager
	 * e la FirstView che viene passata.
	 * 
	 * @param fv Oggetto FirstView che invoca la creazione del controller.
	 */
	public FirstViewController(FirstView fv) {
		fView = fv;	
	}

	/**
	 * Controlla se l'utente e' gia' registrato sulla piattaforma. In caso negativo:
	 * restituisce 9 se non esiste alcun associazione email-password, altrimenti restistuisce 0,1,2 a 
	 * seconda del ruolo dell'utente che effettua il login ed istanzia il rispettivo controller..
	 * 
	 * @param email e-mail utente
	 * @param password Password utente
	 * 
	 * @return int
	 */
	public int login(String email, String password) {

		return userController.login(email, password);

	}

	/**
	 * Registra un nuovo oggetto User sulla piattaforma, se non esiste nel DB.
	 * 
	 * @param email e-mail utente
	 * @param nome Nome utente
	 * @param cognome Cognome utente
	 * @param username Username utente
	 * @param password Password utente
	 * @return int 
	 */
	public int register( String username, String email, String nome, String cognome, String password) {

		return userController.register(username, email, nome, cognome, password);
	}

	/**
	 * @return the u
	 */
	public User getUser() {
		return userController.getUser();
	}

	/**
	 * @param u the u to set
	 */
	public void setUser(User user) {
		userController.setUser(user);
	}

}
