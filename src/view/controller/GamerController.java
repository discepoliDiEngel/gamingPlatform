package view.controller;

import java.io.IOException;
import java.util.TreeMap;

import controller.GameController;
import controller.ReviewController;
import controller.UserController;
import jarRunner.JarRunner;
import model.*;
import view.*;

/**
 * Classe che descrive le operazioni che l'utente giocatore puo' eseguire.
 */
public class GamerController {
	
	TreeMap<String, Review> reviewMap;
	GameController gameController = new GameController();
	UserController userController;
	ReviewController reviewController = new ReviewController();
	GamerView gView;

	/**
	 * Costruttore GamerController: legge i dati dal database immettendo come input l'email utente.
	 * 
	 * @param email e-mail del giocatore che ha eseguito l'accesso alla piattaforma.
	 */
	public GamerController(User s, GamerView view) {
		userController = new UserController(s);
		gView = view;
	}

	/**
	 * Fa partire una sessione di gioco per un determinato gioco passato come parametro.
	 * 
	 * @param game Gioco a cui il giocatore decide di giocare
	 */
	public void play(Game game) {
		try {

			int score = JarRunner.launch(game);

			setEsperienza(score);
			setTimeline(game, score);

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Legge le recensioni scritte per un determinato gioco nel database.
	 * 
	 * @param game Gioco
	 * @return TreeMap<String, Review>
	 */
	public TreeMap<String, Review> getReviews(Game game) {
		return reviewController.visualizzaRecensioni(game);
	}

	/**
	 * Restituisce un vettore contenente le chiavi della mappa, ovvero i titoli delle recensioni.
	 *  
	 * @param reviewMap Mappa da cui estrarre i titoli delle recensioni.
	 * @return String[]
	 */
	public String[] getReviewsTitles() {
		return reviewController.getTitles();
	}
	
	public String[] getReviewsTitles(TreeMap<String, Review> mappa) {

		int i = 0;
		
		String[] arrayTitle = new String[mappa.size()];
		
		for (String title : mappa.keySet()) {
			arrayTitle[i] = title;
			i++;
		}

		return arrayTitle;
	}

	/**
	 * Permette di scrivere una nuova recensione per un gioco.
	 * 
	 * @param title Titolo recensione.
	 * @param author Autore recensione.
	 * @param text Testo recensione.
	 * @param vote Voto recensione.
	 * @param gameID Id gioco per cui e' stata scritta la recensione.
	 */
	public void writeReview(String title, String author, String text, int vote, int gameID) {
		reviewController.writeReview(title, author, text, vote, gameID);
	}
	
	public void setEsperienza(int esperienza) {
		userController.setEsperienza(esperienza);
	}
	
	public void setTimeline(Game game, int exp) {
		userController.setTimeline(game, exp);
	}

	/**
	 * Restituisce l'esperienza acquisita dal giocatore
	 * 
	 * @return esperienza
	 */
	public int getEsperienza() {
		return userController.getEsperienza();
	}

	/**
	 * Restituisce il livello del giocatore
	 * 
	 * @return int
	 */
	public int getLivello() {
		return userController.getLivello();
	}

	/**
	 * @return the games
	 */
	public Game[] getGames() {
		return gameController.getGames();
	}

}
