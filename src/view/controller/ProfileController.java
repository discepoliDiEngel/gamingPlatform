package view.controller;

import controller.UserController;
import model.*;
import view.ProfileView;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class ProfileController {

	ProfileView pView;
	UserController userController;

	
	/**
	 * Costruttore Base.
	 * 
	 * @param gamer Utente per cui creare il controller.
	 * @param exp Esperienza utente.
	 * @param level Livello utente.
	 * @param tipoUser Tipo utente.
	 */
	public ProfileController(User gamer, ProfileView view) {
		pView = view;
		userController = new UserController(gamer);
	}
	
	/**
	 * Aggiorna i dati dell'utente.
	 * 
	 * @param nome Nome utente.
	 * @param cognome Cognome utente.
	 * @param username Username utente.
	 * @param email E-mail utente.
	 * @return boolean
	 */
	public boolean modificaParametri(String nome, String cognome, String username, String email) {
		return userController.modificaParametri(nome, cognome, username, email);
	}
	
	/**
	 * Aggiorna i dati dell'utente compresa la password.
	 * 
	 * @param nome Nome utente.
	 * @param cognome Cognome utente.
	 * @param username Username utente.
	 * @param email E-mail utente.
	 * @param password Nuova password utente.
	 * @return boolean
	 */
	public boolean modificaParametriConPsw(String nome, String cognome, String username, String email, String password) {
		return userController.modificaParametriConPsw(nome, cognome, username, email, password);
	}

}
