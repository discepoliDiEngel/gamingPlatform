package view.controller;

import java.util.ArrayList;

import controller.UserController;
import model.*;
import view.AdminView;

/**
 * Controller utente Amministratore/Admin. 
 *
 * @author Discepoli di Engel
 */
public class AdministratorController extends ModeratorController {

	UserController userController = new UserController();
	private AdminView aView;

	/**
	 * Costruttore base.
	 */
	public AdministratorController(AdminView view) {

		setaView(view);
		
	}

	/**
	 * Promuove un utente base al ruolo di utente moderatore.
	 * 
	 * @return boolean
	 */
	public boolean promuovi (User user) {	
		return userController.promuovi(user);
	}

	/**
	 * Retrocede un utente da Moderatore a Utente base.
	 * 
	 * @return boolean
	 */
	public boolean retrocedi (User user) {
		return userController.retrocedi(user);
	}
	
	/**
	 * Elimina un utente dalla piattaforma.
	 * 
	 * @param user Utente da eliminare.
	 * @return boolean
	 */
	public boolean eliminaUtente(User user) {
		return userController.eliminaUtente(user);
	}

	/**
	 * @return the aView
	 */
	public AdminView getaView() {
		return aView;
	}

	/**
	 * @param aView the aView to set
	 */
	public void setaView(AdminView aView) {
		this.aView = aView;
	}

	/**
	 * @return the usersList
	 */
	public ArrayList<User> getUsersList() {
		return userController.getUsersList();
	}

	/**
	 * @param usersList the usersList to set
	 */
	public void setUsersList(ArrayList<User> usersList) {
		userController.setUsersList(usersList);
	}

	/**
	 * @return the users
	 */
	public User[] getUsers() {
		return userController.getUsers();
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(User[] users) {
		userController.setUsers(users);
	}
	
	/**
	 * @return the users
	 */
	public String[] getTitles() {
		return reviewController.getTitles();
	}

	/**
	 * @param users the users to set
	 */
	public void setTitles(String[] titles) {
		reviewController.setTitles(titles);
	}

}
