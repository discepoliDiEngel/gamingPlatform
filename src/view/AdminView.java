package view;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.TreeMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import model.*;
import view.controller.AdministratorController;
import view.renderer.UserCellRenderer;

/**
 * Finestra con cui interagisce l'amministratore della piattaforma.
 * 
 * @author Discepoli di Engel
 *
 */
public class AdminView {
	
	private JFrame frmAdmin;
	
	private AdministratorController controller;

	/**
	 * Crea la vista.
	 * 
	 * @param contr Controller Amministratore
	 * @param reviewTree Mappa in cui e' contenuta la coppia <Titotlo, Recensione>
	 * @param arrayTitle Vettore che contiene i titoli delle Recensioni.
	 * @param users Vettore in cui sono contenuti gli utenti iscritti alla piattaforma.
	 */
	public AdminView() {
		
		controller = new AdministratorController(this);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					initialize(controller.getReviewTree(), controller.getTitles(), controller.getUsers());
					frmAdmin.setVisible(true);
					
				} catch (Exception e) {	
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Inizializza i contenuti del frame.
	 * 
	 * @param reviewTree Mappa in cui e' contenuta la coppia <Titotlo, Recensione>
	 * @param arrayTitle Vettore che contiene i titoli delle Recensioni.
	 * @param users Vettore in cui sono contenuti gli utenti iscritti alla piattaforma.
	 */
	@SuppressWarnings("unchecked")
	private void initialize(TreeMap<String, Review> reviewTree, String[] arrayTitle, User[] users) {
		
		/*
		 * Set pulsanti.
		 */
		JButton btnAcceptComment = new JButton("Approva");
		JButton btnDiscardComment = new JButton("Elimina");
		JButton btnPromuoviUser = new JButton("Promuovi");
		JButton btnDeclassaUser = new JButton("Declassa");

		/*
		 * Set delle immagini
		 */
		ImageIcon backgroundAdmin = new ImageIcon("resources/img/wallpaper/wp02.jpg");
		ImageIcon section = new ImageIcon("resources/img/admin/listSectionAdmin.png");
		ImageIcon topBar = new ImageIcon("resources/img/admin/TitleBar.png");
		ImageIcon topBarBottom2 = new ImageIcon("resources/img/admin/TitleBarBottom2.png");
		ImageIcon footerBar = new ImageIcon("resources/img/admin/FooterBar.png");
		ImageIcon logo = new ImageIcon("resources/img/admin/boltLogo.png");
		ImageIcon user = new ImageIcon("resources/img/admin/userImage.png");
		ImageIcon logout = new ImageIcon("resources/img/admin/exitButton.png");
		ImageIcon cestino = new ImageIcon("resources/img/admin/cestino.png");
		ImageIcon topBarBottom = new ImageIcon("resources/img/admin/TitleBarBottom.png");
		JButton btnEliminaUser = new JButton(cestino);

		/*
		 * JFrame
		 */
		frmAdmin = new JFrame();
		frmAdmin.setTitle("BOLT Gaming Platform - Admin");
		frmAdmin.setBounds(100, 0, 1280, 820);
		frmAdmin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAdmin.getContentPane().setLayout(null);
		frmAdmin.setResizable(false);
		frmAdmin.setLocationRelativeTo(null);

		/*
		 * Label: commenti
		 */
		JLabel lblReview = new JLabel("Review");
		lblReview.setForeground(Color.WHITE);
		lblReview.setHorizontalAlignment(SwingConstants.CENTER);
		lblReview.setBorder(null);
		lblReview.setBounds(40, 74, 400, 26);
		frmAdmin.getContentPane().add(lblReview);

		/*
		 * Label: commenti dettagliati
		 */
		JLabel lblDetailReview = new JLabel("Detail Review");
		lblDetailReview.setForeground(Color.WHITE);
		lblDetailReview.setHorizontalAlignment(SwingConstants.CENTER);
		lblDetailReview.setBorder(null);
		lblDetailReview.setBounds(440, 74, 400, 26);
		frmAdmin.getContentPane().add(lblDetailReview);

		/*
		 * Label: user
		 */
		JLabel lblUser = new JLabel("User");
		lblUser.setForeground(Color.WHITE);
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setBorder(null);
		lblUser.setBounds(840, 74, 400, 26);
		frmAdmin.getContentPane().add(lblUser);

		/*
		 * Label: titolo review 
		 */
		JLabel lblReviewTitle = new JLabel("Review Title");
		lblReviewTitle.setForeground(Color.WHITE);
		lblReviewTitle.setHorizontalAlignment(SwingConstants.LEFT);
		lblReviewTitle.setBorder(null);
		lblReviewTitle.setBounds(450, 110, 190, 30);
		frmAdmin.getContentPane().add(lblReviewTitle);

		/*
		 * Label: voto
		 */
		JLabel lblVoto = new JLabel("Voto_Integer");
		lblVoto.setForeground(Color.WHITE);
		lblVoto.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVoto.setBorder(null);
		lblVoto.setBounds(640, 110, 190, 30);
		frmAdmin.getContentPane().add(lblVoto);

		/*
		 * Label: username
		 */
		JLabel lblNicknameList = new JLabel("Nickname User");
		lblNicknameList.setForeground(Color.WHITE);
		lblNicknameList.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNicknameList.setBorder(null);
		lblNicknameList.setBounds(640, 170, 190, 30);
		frmAdmin.getContentPane().add(lblNicknameList);

		/*
		 * Detail: barra review
		 */
		JLabel lblDetailReviewBar = new JLabel("New label");
		lblDetailReviewBar = new JLabel(topBarBottom2);
		lblDetailReviewBar.setBorder(null);
		lblDetailReviewBar.setBounds(440, 256, 440, 4);
		frmAdmin.getContentPane().add(lblDetailReviewBar);

		/*
		 * Label: Welcome
		 */
		JLabel lblWelcome = new JLabel("Benvenuto");
		lblWelcome.setForeground(Color.WHITE);
		lblWelcome.setHorizontalAlignment(SwingConstants.RIGHT);
		lblWelcome.setBorder(null);
		lblWelcome.setBounds(1000, 10, 160, 20);
		frmAdmin.getContentPane().add(lblWelcome);

		/*
		 * Label: username utente
		 */
		JLabel lblNickUtente = new JLabel("Administrator");
		lblNickUtente.setForeground(Color.WHITE);
		lblNickUtente.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNickUtente.setBorder(null);
		lblNickUtente.setBounds(1000, 30, 160, 20);
		frmAdmin.getContentPane().add(lblNickUtente);

		/*
		 * Image User
		 */
		JLabel lblUserImage = new JLabel("New label");
		lblUserImage = new JLabel(user);
		lblUserImage.setBorder(null);
		lblUserImage.setBounds(1170, 10, 40, 40);
		frmAdmin.getContentPane().add(lblUserImage);

		/*
		 * Pulsante: Logout
		 */
		JButton btnLogout = new JButton("");
		btnLogout = new JButton(logout);
		btnLogout.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				frmAdmin.setVisible(false);
				frmAdmin.dispose();
				new FirstView();

			}
		});
		btnLogout.setBorder(null);
		btnLogout.setBounds(1230, 10, 40, 40);
		frmAdmin.getContentPane().add(btnLogout);

		/*
		 * Logo Bolt
		 */
		JLabel lblLogo = new JLabel("New label");
		lblLogo = new JLabel(logo);
		lblLogo.setBorder(null);
		lblLogo.setBounds(580, 0, 120, 58);
		frmAdmin.getContentPane().add(lblLogo);

		/*
		 * Top Bar
		 */
		JLabel lblTopBar = new JLabel("New label");
		lblTopBar = new JLabel(topBar);
		lblTopBar.setBorder(null);
		lblTopBar.setBounds(0, 0, 1280, 60);
		frmAdmin.getContentPane().add(lblTopBar);

		/*
		 * Top Bar 2
		 */
		JLabel lblTopBarBottom = new JLabel("New label");
		lblTopBarBottom = new JLabel(topBarBottom);
		lblTopBarBottom.setBorder(null);
		lblTopBarBottom.setBounds(0, 60, 1280, 4);
		frmAdmin.getContentPane().add(lblTopBarBottom);

		/*
		 * Footer Bar
		 */
		JLabel lblFooterBar = new JLabel("Copyright @2017 - BOLT");
		lblFooterBar = new JLabel(footerBar);
		lblFooterBar.setForeground(Color.WHITE);
		lblFooterBar.setHorizontalTextPosition(SwingConstants.CENTER);
		lblFooterBar.setText("Copyright 2017 - BOLT");
		lblFooterBar.setBorder(null);
		lblFooterBar.setBounds(0, 760, 1280, 40);
		frmAdmin.getContentPane().add(lblFooterBar);

		/*
		 * Center Review
		 */
		JTextArea textPane = new JTextArea();
		textPane.setMargin(new Insets(10, 15, 10, 15));
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		textPane.setForeground(Color.WHITE);
		textPane.setBounds(440, 260, 400, 440);
		textPane.setBackground(new Color(33, 33, 33, 255));
		frmAdmin.getContentPane().add(textPane);
		textPane.setOpaque(true);
		textPane.setLineWrap(true);

		/*
		 * Lista Sinistra
		 */
		frmAdmin.getContentPane().setLayout(null);
		frmAdmin.getContentPane().setLayout(null);
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setOpaque(false);
		scrollPane1.setPreferredSize(new Dimension (500, 100));
		scrollPane1.setLocation(40, 100);
		frmAdmin.getContentPane().setLayout(null);
		scrollPane1.setBounds(40,100, 400, 600);
		frmAdmin.getContentPane().add(scrollPane1);
		scrollPane1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

		/*
		 * Lista titoli recensioni.
		 */
		JList<String> list1 = new JList<String>(arrayTitle);
		scrollPane1.setViewportView(list1);
		list1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				String titleReview = (String) list1.getSelectedValue();
				System.out.println("Ho cliccato " + titleReview);
				
				Review reviewTemp = reviewTree.get(titleReview);
				
				//Label: titolo review
				lblReviewTitle.setText(reviewTemp.getTitle());
				
				// Label: testo della review
				textPane.setText(reviewTemp.getText());
				
				//Label: voto
				lblVoto.setText("Voto: "+ reviewTemp.getVote());
				
				//Label: authors' username
				lblNicknameList.setText(reviewTemp.getUsername());
				
				btnAcceptComment.setEnabled(true);
				btnDiscardComment.setEnabled(true);
				list1.setSelectionBackground(new Color(2, 119, 189));
				
				frmAdmin.validate();
				frmAdmin.repaint();
				
			}
		});
		list1.setSelectionBackground(new Color(2, 119, 189));
		list1.setFont(new Font("Menlo", Font.PLAIN, 14));
		frmAdmin.setResizable(false);

		/*
		 * Lista Destra
		 */
		frmAdmin.getContentPane().setLayout(null);
		frmAdmin.getContentPane().setLayout(null);
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setOpaque(false);
		scrollPane2.setPreferredSize(new Dimension (500, 100));
		scrollPane2.setLocation(840, 100);
		scrollPane2.setBounds(840, 100, 400, 600);
		frmAdmin.getContentPane().add(scrollPane2);
		scrollPane2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

		/*
		 * Lista utenti.
		 */
		JList<User> list2 = new JList<User> (users);
		list2.setCellRenderer(new UserCellRenderer());
		scrollPane2.setViewportView(list2);
		list2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				//System.out.println("Click :" + list2.getSelectedValue());
				btnPromuoviUser.setEnabled(true);
				btnDeclassaUser.setEnabled(true);
				btnEliminaUser.setEnabled(true);
				list2.setSelectionBackground(new Color(2, 119, 189));
				
				//Label: nome e cognome del utente
				lblReviewTitle.setText(list2.getSelectedValue().getNome() + " " + list2.getSelectedValue().getCognome());
				
				//Label: ruolo
				lblVoto.setText(list2.getSelectedValue().getRole());
				
				//Label: email
				lblNicknameList.setText(list2.getSelectedValue().getEmail());
				
				frmAdmin.validate();
				frmAdmin.repaint();
				
			}
		});
		list2.setSelectionBackground(new Color(2, 119, 189));
		list2.setOpaque(false);
		list2.setFont(new Font("Menlo", Font.PLAIN, 14));
		frmAdmin.setResizable(false);	

		/*
		 * Pulsante: promozione utente
		 */
		btnPromuoviUser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				System.out.println("Hai promosso: " + list2.getSelectedValue().getNome() + " " + list2.getSelectedValue().getCognome());
				User selectedUser = list2.getSelectedValue();
				controller.promuovi(selectedUser);
				
				list2.setSelectionBackground(new Color(76,175,80));
				list2.validate();
				list2.repaint();

			}
		});	
		btnPromuoviUser.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnPromuoviUser.setForeground(Color.WHITE);
		btnPromuoviUser.setBounds(840, 700, 160, 40);
		frmAdmin.getContentPane().add(btnPromuoviUser);
		btnPromuoviUser.setBorderPainted(false);
		btnPromuoviUser.setBackground(new Color(2, 136, 209));
		btnPromuoviUser.setOpaque(true);
		btnPromuoviUser.setEnabled(false);

		/*
		 * Pulsante: declassa user
		 */
		btnDeclassaUser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				System.out.println("Ho declassato " + list2.getSelectedValue());
				User valSelected = list2.getSelectedValue();
				controller.retrocedi(valSelected);
				
				list2.setSelectionBackground(new Color(255,152,0));
				list2.validate();
				list2.repaint();
				
			}
		});
		btnDeclassaUser.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnDeclassaUser.setForeground(Color.WHITE);
		btnDeclassaUser.setBounds(1000, 700, 160, 40);
		frmAdmin.getContentPane().add(btnDeclassaUser);
		btnDeclassaUser.setBorderPainted(false);
		btnDeclassaUser.setBackground(new Color(2, 119, 189));
		btnDeclassaUser.setOpaque(true);
		btnDeclassaUser.setEnabled(false);

		/*
		 * Pulsante: elimina user
		 */
		btnEliminaUser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				System.out.println("Hai eliminato: " + list2.getSelectedValue());
				User utente = list2.getSelectedValue();
				controller.eliminaUtente(utente);
				
				list2.setSelectionBackground(new Color(255,87,34));
				list2.validate();
				list2.repaint();
				
			}
		});
		btnEliminaUser.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnEliminaUser.setForeground(Color.WHITE);
		btnEliminaUser.setBounds(1160, 700, 80, 40);
		frmAdmin.getContentPane().add(btnEliminaUser);
		btnEliminaUser.setBorderPainted(false);
		btnEliminaUser.setBackground(new Color(221, 44, 0));
		btnEliminaUser.setOpaque(true);
		btnEliminaUser.setEnabled(false);

		/*
		 * Pulsante: accetta commento
		 */
		btnAcceptComment.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				String titleReview = (String) list1.getSelectedValue();
				int index = list1.getSelectedIndex();
				
				textPane.setText("Hai approvato: " + titleReview);
				
				controller.approvaRecensione(reviewTree.get(titleReview));
				System.out.println("Ho approvato il commento " + titleReview);
				reviewTree.remove(titleReview);
				arrayTitle[index] = "";
				
				list1.validate();
				list1.repaint();
				
			}
		});	
		btnAcceptComment.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnAcceptComment.setForeground(Color.WHITE);
		btnAcceptComment.setBounds(440, 700, 200, 40);
		frmAdmin.getContentPane().add(btnAcceptComment);
		btnAcceptComment.setBorderPainted(false);
		btnAcceptComment.setBackground(new Color(2, 136, 209));
		btnAcceptComment.setOpaque(true);
		btnAcceptComment.setEnabled(false);


		/*
		 * Pulsante: elimina commento
		 */
		btnDiscardComment.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				String titleReview = (String)list1.getSelectedValue();
				int index = list1.getSelectedIndex();

				textPane.setText("Hai eliminato: " + titleReview);

				controller.eliminaReview(reviewTree.get(titleReview));
				System.out.println("Ho eliminato il commento " + list1.getSelectedValue());

				reviewTree.remove(titleReview);
				arrayTitle[index] = "";
				list1.validate();
				list1.repaint();
				
			}
		});
		btnDiscardComment.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnDiscardComment.setForeground(Color.WHITE);
		btnDiscardComment.setBounds(640, 700, 200, 40);
		frmAdmin.getContentPane().add(btnDiscardComment);
		btnDiscardComment.setBorderPainted(false);
		btnDiscardComment.setBackground(new Color(2, 119, 189));
		btnDiscardComment.setOpaque(true);
		btnDiscardComment.setEnabled(false);		

		/*
		 * Immagine sezione sinistra
		 */
		JLabel lblSectionLeft = new JLabel("New label");
		lblSectionLeft = new JLabel(section);
		lblSectionLeft.setBorder(null);
		lblSectionLeft.setBounds(40, 100, 400, 600);
		frmAdmin.getContentPane().add(lblSectionLeft);

		/*
		 * Immagine sezione centrale
		 */
		JLabel lblSectionCenter = new JLabel("New label");
		lblSectionCenter = new JLabel(section);
		lblSectionCenter.setBorder(null);
		lblSectionCenter.setBounds(440, 100, 400, 600);
		frmAdmin.getContentPane().add(lblSectionCenter);

		/*
		 * Immagine sezione destra
		 */
		JLabel lblSectionRight = new JLabel("New label");
		lblSectionRight = new JLabel(section);
		lblSectionRight.setBorder(null);
		lblSectionRight.setBounds(840, 100, 400, 600);
		frmAdmin.getContentPane().add(lblSectionRight);

		/*
		 * Immagine sfondo
		 */
		JLabel lblSfondo = new JLabel("New label");
		lblSfondo = new JLabel(backgroundAdmin);
		lblSfondo.setBorder(null);
		lblSfondo.setBounds(0, 0, 1280, 800);
		frmAdmin.getContentPane().add(lblSfondo);

	}
	
}
