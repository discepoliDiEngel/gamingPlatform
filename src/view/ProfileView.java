package view;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import model.Levels;
import model.Review;
import model.User;
import view.controller.ModeratorController;
import view.controller.ProfileController;

/**
 * Vista profilo utente.
 * 
 * @author Discepoli di Engel
 *
 */
@SuppressWarnings("rawtypes")
public class ProfileView implements Levels{

	private ProfileController controller;
	
	private ModeratorController moderController;

	private JFrame frame;
	private JTextField txtNome;
	private JTextField txtCognome;
	private JTextField txtEmail;
	private JTextField txtNick;

	/*
	 * Serve a non far svuotare i campi piu- di una volta.
	 */
	boolean svuotaNome = false;
	boolean svuotaCognome = false;
	boolean svuotaNick = false;
	boolean svuotaEmail = false;
	boolean svuotaPwd = false;
	boolean svuotaConfermaPwd = false;
	boolean svuotaEmailNick = false;
	boolean svuotaLoginPwd = false;

	/*
	 * Dichiarazione globale oggetti.
	 */
	JButton btnTimeline = new JButton("Timeline");
	JButton btnModerate = new JButton("Moderate");
	JButton btnApprova = new JButton("Approva");
	JButton btnElimina = new JButton("Elimina");
	JLabel lblTimeline = new JLabel("Timeline");
	JList timelineGamer;
	JList timelineModeratore;
	JTextArea txtDescrizione = new JTextArea("Descrizione");
	JTextArea lblCommento = new JTextArea("Commento");
	JList<String> commentiModeratore;

	/*
	 * Dichiarazione per le stelle punteggio.
	 */
	String star0 = "";
	String star1 = "";
	String star2 = "";
	String star3 = "";
	String star4 = "";

	String vecchiaEmail = "";

	/**
	 * Crea la vista.
	 * 
	 * @param contr Controller profilo utente.
	 * @param gamer Utente che invoca la view.
	 * @param exp Esperienza utente.
	 * @param level Livello utente.
	 * @param tipoUser Tipo di utente che accede al profilo.
	 */
	public ProfileView(User gamer, String tipoUser) {
		
		controller = new ProfileController(gamer, this);
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize(gamer, gamer.getExperience(), gamer.calcLevel(), tipoUser);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Inizilizza il contenuto del frame.
	 * 
	 * @param gamer Utente che invoca la view.
	 * @param exp Esperienza utente.
	 * @param level Livello utente.
	 * @param tipoUser Tipo di utente che accede al profilo.
	 */
	@SuppressWarnings("unchecked")
	private void initialize(User gamer, int exp, int level, String tipoUser) {

		moderController = new ModeratorController();
		List lista= gamer.getTimeline();
		timelineGamer = new JList(lista.toArray(new String[lista.size()]));
		timelineModeratore = new JList(gamer.getTimeline().toArray());

		cambiaStella(gamer.getExperience());
		verificaUser(tipoUser);

		/*
		 * Icone e immagini usate all'interno del frame.
		 */
		ImageIcon footerBar = new ImageIcon("resources/img/admin/FooterBar.png");
		ImageIcon topBar = new ImageIcon("resources/img/admin/TitleBar.png");
		ImageIcon topBarBottom = new ImageIcon("resources/img/admin/TitleBarBottom.png");
		ImageIcon logo = new ImageIcon("resources/img/admin/boltLogo.png");
		ImageIcon barre = new ImageIcon("resources/img/profileView/Barre.png");
		ImageIcon buttonBack = new ImageIcon("resources/img/profileView/back.png");
		ImageIcon sfondo = new ImageIcon("resources/img/wallpaper/wp01.jpg");
		ImageIcon section = new ImageIcon("resources/img/profileView/Section.png");
		ImageIcon userImage = new ImageIcon("resources/img/gamer/rajeshUser.png");
		ImageIcon varstar0 = new ImageIcon(star0);
		ImageIcon varstar1 = new ImageIcon(star1);
		ImageIcon varstar2 = new ImageIcon(star2);
		ImageIcon varstar3 = new ImageIcon(star3);
		ImageIcon varstar4 = new ImageIcon(star4);

		frame = new JFrame();
		frame.setTitle("BOLT Gaming Platform - Profilo Utente");
		frame.setBounds(100, 0, 1280, 820);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		/*
		 * Timeline
		 */
		btnTimeline.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnTimeline.setForeground(Color.BLACK);
		btnTimeline.setBorderPainted(false);
		btnTimeline.setBackground(new Color(255, 193, 80));
		btnTimeline.setOpaque(true);
		btnTimeline.setBounds(840, 100, 200, 40);
		frame.getContentPane().add(btnTimeline);

		/*
		 * Pulsante riservato al moderatore.
		 */
		btnModerate.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnModerate.setForeground(Color.BLACK);
		btnModerate.setBorderPainted(false);
		btnModerate.setBackground(new Color(255, 193, 80));
		btnModerate.setOpaque(true);
		btnModerate.setBounds(1040, 100, 200, 40);
		frame.getContentPane().add(btnModerate);

		/*
		 * Bottone approva recensione.
		 */
		btnApprova.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnApprova.setForeground(Color.BLACK);
		btnApprova.setBorderPainted(false);
		btnApprova.setBackground(new Color(255, 193, 80));
		btnApprova.setOpaque(true);
		btnApprova.setVisible(false);
		btnApprova.setBounds(840, 700, 200, 40);
		frame.getContentPane().add(btnApprova);

		/*
		 * Bottone elimina recensione
		 */
		btnElimina.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnElimina.setForeground(Color.BLACK);
		btnElimina.setBorderPainted(false);
		btnElimina.setBackground(new Color(255, 193, 80));
		btnElimina.setOpaque(true);
		btnElimina.setVisible(false);
		btnElimina.setBounds(1040, 700, 200, 40);
		frame.getContentPane().add(btnElimina);

		/*
		 * Timeline Gamer, Profilo Gamer
		 */
		timelineGamer.setBorder(null);
		timelineGamer.setSelectionBackground(new Color(231, 76, 60));
		timelineGamer.setOpaque(false);
		frame.getContentPane().setLayout(null);
		timelineGamer.setBounds(840, 140, 400, 560);
		frame.getContentPane().add(timelineGamer);
		timelineGamer.setFont(new Font("Menlo", Font.PLAIN, 14));
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setVisible(false);
		scrollPane1.setOpaque(false);
		scrollPane1.setPreferredSize(new Dimension(500, 100));
		scrollPane1.setLocation(40, 100);
		scrollPane1.setBounds(40, 100, 400, 600);
		frame.getContentPane().add(scrollPane1);
		scrollPane1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		frame.setResizable(false);

		/*
		 * Timeline Moderatore, Profilo Moderatore
		 */
		timelineModeratore.setVisible(false);
		timelineModeratore.setBorder(null);
		timelineModeratore.setSelectionBackground(new Color(231, 76, 60));
		timelineModeratore.setOpaque(false);
		frame.getContentPane().setLayout(null);
		timelineModeratore.setBounds(840, 140, 400, 560);
		frame.getContentPane().add(timelineModeratore);
		timelineModeratore.setFont(new Font("Menlo", Font.PLAIN, 14));
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setVisible(false);
		scrollPane2.setOpaque(false);
		scrollPane2.setPreferredSize(new Dimension(500, 100));
		scrollPane2.setLocation(40, 100);
		scrollPane2.setBounds(40, 100, 400, 600);
		frame.getContentPane().add(scrollPane2);
		scrollPane2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		frame.setResizable(false);

		/*
		 * Legge vettore recensioni dal db.
		 */
		String[] arrayTitle = moderController.getArrayTitle();
		TreeMap<String, Review> reviewTree = moderController.getReviewTree();
		commentiModeratore = new JList<String>(arrayTitle);

		/*
		 * Timeline Commenti Moderatore, Profilo Moderatore
		 */
		commentiModeratore.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//System.out.println("Ho cliccato " + commentiModeratore.getSelectedValue());
				commentiModeratore.setSelectionBackground(new Color(2, 119, 189));
				lblCommento.setText(reviewTree.get((String) commentiModeratore.getSelectedValue()).getText());
				frame.validate();
				frame.repaint();
			}
		});
		commentiModeratore.setVisible(false);
		commentiModeratore.setBorder(null);
		commentiModeratore.setSelectionBackground(new Color(231, 76, 60));
		commentiModeratore.setOpaque(false);
		frame.getContentPane().setLayout(null);
		commentiModeratore.setBounds(840, 140, 400, 560);
		frame.getContentPane().add(commentiModeratore);
		commentiModeratore.setFont(new Font("Menlo", Font.PLAIN, 14));
		JScrollPane scrollPane3 = new JScrollPane();
		scrollPane3.setVisible(false);
		scrollPane3.setOpaque(false);
		scrollPane3.setPreferredSize(new Dimension(500, 100));
		scrollPane3.setLocation(40, 100);
		scrollPane3.setBounds(40, 100, 400, 600);
		frame.getContentPane().add(scrollPane3);
		scrollPane3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		frame.setResizable(false);

		/*
		 * Label Timeline.
		 */
		lblTimeline.setBorder(null);
		lblTimeline.setBounds(1120, 108, 80, 26);
		lblTimeline.setFont(new Font("Futura", Font.PLAIN, 20));
		lblTimeline.setForeground(Color.BLACK);
		frame.getContentPane().add(lblTimeline);

		/*
		 * Label Barre.
		 */
		JLabel lblBarre = new JLabel("New label");
		lblBarre = new JLabel(barre);
		lblBarre.setBorder(null);
		lblBarre.setBounds(460, 140, 340, 120);
		frame.getContentPane().add(lblBarre);

		/*
		 * Logo Bolt.
		 */
		JLabel lblLogo = new JLabel("New label");
		lblLogo = new JLabel(logo);
		lblLogo.setBorder(null);
		lblLogo.setBounds(580, 0, 120, 58);
		frame.getContentPane().add(lblLogo);

		/*
		 * Pulsante Back.
		 */
		JButton btnBack = new JButton("New button");
		btnBack = new JButton(buttonBack);
		btnBack.setBorder(null);
		btnBack.setBounds(20, 10, 40, 40);
		frame.getContentPane().add(btnBack);

		/*
		 * Top Bar.
		 */
		JLabel lblTopBar = new JLabel("New label");
		lblTopBar = new JLabel(topBar);
		lblTopBar.setBorder(null);
		lblTopBar.setBounds(0, 0, 1280, 60);
		frame.getContentPane().add(lblTopBar);

		/*
		 * Top Bar 2
		 */
		JLabel lblTopBarBottom = new JLabel("New label");
		lblTopBarBottom = new JLabel(topBarBottom);
		lblTopBarBottom.setBorder(null);
		lblTopBarBottom.setBounds(0, 60, 1280, 4);
		frame.getContentPane().add(lblTopBarBottom);

		/*
		 * TXT Nome
		 */
		txtNome = new JTextField();
		txtNome.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtNome.setForeground(Color.WHITE);
		txtNome.setBackground(new Color(224, 224, 224, 150));
		txtNome.setBounds(80, 310, 300, 40);
		txtNome.setBorder(null);
		txtNome.setEnabled(false);
		txtNome.setBorder(
				BorderFactory.createCompoundBorder(txtNome.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtNome);
		txtNome.setText(gamer.getNome());

		/*
		 * TXT Cognome
		 */
		txtCognome = new JTextField();
		txtCognome.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtCognome.setForeground(Color.WHITE);
		txtCognome.setBackground(new Color(224, 224, 224, 150));
		txtCognome.setBounds(80, 370, 300, 40);
		txtCognome.setBorder(null);
		txtCognome.setEnabled(false);
		txtCognome.setBorder(BorderFactory.createCompoundBorder(txtCognome.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtCognome);
		txtCognome.setText(gamer.getCognome());

		/*
		 * TXT Nick
		 */
		txtNick = new JTextField();
		txtNick.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtNick.setForeground(Color.WHITE);
		txtNick.setBackground(new Color(224, 224, 224, 150));
		txtNick.setBounds(80, 430, 300, 40);
		txtNick.setBorder(null);
		txtNick.setEnabled(false);
		txtNick.setBorder(
				BorderFactory.createCompoundBorder(txtNick.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtNick);
		txtNick.setText(gamer.getUsername());

		/*
		 * TXT Email
		 */
		txtEmail = new JTextField();
		txtEmail.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtEmail.setForeground(Color.WHITE);
		txtEmail.setBackground(new Color(224, 224, 224, 150));
		txtEmail.setBounds(80, 490, 300, 40);
		txtEmail.setBorder(null);
		txtEmail.setEnabled(false);
		txtEmail.setBorder(
				BorderFactory.createCompoundBorder(txtEmail.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtEmail);
		txtEmail.setText(gamer.getEmail());

		/*
		 * TextField - Password
		 */
		JPasswordField txtPwd = new JPasswordField(20);
		txtPwd.setForeground(Color.WHITE);
		txtPwd.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtPwd.setBackground(new Color(224, 224, 224, 150));
		txtPwd.setBounds(80, 550, 300, 40);
		txtPwd.setBorder(null);
		txtPwd.setEchoChar((char) 0);
		txtPwd.setEnabled(false);
		txtPwd.setBorder(
				BorderFactory.createCompoundBorder(txtPwd.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtPwd);
		txtPwd.setText("Password");

		/*
		 * TextField - Conferma Password
		 */
		JPasswordField txtConfermaPwd = new JPasswordField(20);
		txtConfermaPwd.setForeground(Color.WHITE);
		txtConfermaPwd.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
		txtConfermaPwd.setBounds(80, 610, 300, 40);
		txtConfermaPwd.setBorder(null);
		txtConfermaPwd.setEchoChar((char) 0);
		txtConfermaPwd.setEnabled(false);
		txtConfermaPwd.setBorder(BorderFactory.createCompoundBorder(txtConfermaPwd.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtConfermaPwd);
		txtConfermaPwd.setText("Conferma Password");

		/*
		 * Visualizza il commento una volta cliccato.
		 */
		lblCommento = new JTextArea();
		lblCommento.setBorder(null);
		lblCommento.setLineWrap(true);
		lblCommento.setBounds(840, 420, 400, 280);
		lblCommento.setBackground(new Color(236, 240, 241));
		lblCommento.setEditable(false);
		lblCommento.setOpaque(false);
		lblCommento.setVisible(false);
		frame.getContentPane().add(lblCommento);

		/*
		 * Pulsante modifica
		 */
		JButton btnEdit = new JButton("Modifica Dati");
		btnEdit.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				btnEdit.setBackground(new Color(46, 204, 113));
			}

			public void mouseExited(MouseEvent me) {
				btnEdit.setBackground(new Color(255, 193, 80));
			}
		});
		btnEdit.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnEdit.setForeground(Color.BLACK);
		btnEdit.setBorderPainted(false);
		btnEdit.setBackground(new Color(255, 193, 80));
		btnEdit.setOpaque(true);
		btnEdit.setBounds(600, 610, 200, 40);
		frame.getContentPane().add(btnEdit);

		/*
		 * Pulsante Ok
		 */
		JButton btnOk = new JButton("Conferma Dati");
		btnOk.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				btnOk.setBackground(new Color(46, 204, 113));
			}

			public void mouseExited(MouseEvent me) {
				btnOk.setBackground(new Color(255, 193, 80));
			}
		});
		btnOk.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnOk.setForeground(Color.BLACK);
		btnOk.setBorderPainted(false);
		btnOk.setBackground(new Color(255, 193, 80));
		btnOk.setOpaque(true);
		btnOk.setEnabled(false);
		btnOk.setVisible(false);
		btnOk.setBounds(600, 550, 200, 40);
		frame.getContentPane().add(btnOk);

		/*
		 * LBL User
		 */
		JLabel lblUserPageText = new JLabel("User Page");
		lblUserPageText.setForeground(Color.WHITE);
		lblUserPageText.setHorizontalAlignment(SwingConstants.LEFT);
		lblUserPageText.setFont(new Font("Menlo", Font.PLAIN, 24));
		lblUserPageText.setBounds(460, 310, 340, 40);
		frame.getContentPane().add(lblUserPageText);
		lblUserPageText.setText("Benvenuto " + gamer.getUsername() + "!");

		/*
		 * LBL User
		 */
		txtDescrizione = new JTextArea("User Page");
		txtDescrizione.setForeground(Color.WHITE);
		txtDescrizione.setLineWrap(true);
        txtDescrizione.setWrapStyleWord(true);
		txtDescrizione.setFont(new Font("Menlo", Font.PLAIN, 16));
		txtDescrizione.setBounds(460, 350, 340, 200);
		txtDescrizione.setOpaque(false);
		frame.getContentPane().add(txtDescrizione);
		txtDescrizione.setText(
				"Questa e' la tua pagina dove puoi gestire il tuo Profilo da Gamer.\nClicca il pulsante in basso per modificare le tue informazioni personali.\nAlla tua destra invece troverai informazioni riguardo il punteggio e trofei conquistati.\nDistruggi i tuoi avversari!!!");

		/*
		 * LBL Punteggio
		 */
		JLabel lblPunteggio = new JLabel(gamer.getExperience() + " PT");
		lblPunteggio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPunteggio.setForeground(Color.WHITE);
		lblPunteggio.setFont(new Font("Menlo", Font.PLAIN, 48));
		lblPunteggio.setBounds(270, 140, 190, 60);
		frame.getContentPane().add(lblPunteggio);

		/*
		 * LBL Star1
		 */
		JLabel lblStar01 = new JLabel("New label");
		lblStar01 = new JLabel(varstar0);
		lblStar01.setBorder(null);
		lblStar01.setBounds(270, 230, 30, 30);
		frame.getContentPane().add(lblStar01);

		/*
		 * LBL Star2
		 */
		JLabel lblStar02 = new JLabel("New label");
		lblStar02 = new JLabel(varstar1);
		lblStar02.setBorder(null);
		lblStar02.setBounds(305, 230, 30, 30);
		frame.getContentPane().add(lblStar02);

		/*
		 * LBL Star3
		 */
		JLabel lblStar03 = new JLabel("New label");
		lblStar03 = new JLabel(varstar2);
		lblStar03.setBorder(null);
		lblStar03.setBounds(340, 230, 30, 30);
		frame.getContentPane().add(lblStar03);

		/*
		 * LBL Star4
		 */
		JLabel lblStar04 = new JLabel("New label");
		lblStar04 = new JLabel(varstar3);
		lblStar04.setBorder(null);
		lblStar04.setBounds(375, 230, 30, 30);
		frame.getContentPane().add(lblStar04);

		/*
		 * LBL Star5
		 */
		JLabel lblStar05 = new JLabel("New label");
		lblStar05 = new JLabel(varstar4);
		lblStar05.setBorder(null);
		lblStar05.setBounds(410, 230, 30, 30);
		frame.getContentPane().add(lblStar05);

		/*
		 * LBL ImageUser
		 */
		JLabel lblImageUser = new JLabel("New label");
		lblImageUser = new JLabel(userImage);
		lblImageUser.setBorder(null);
		lblImageUser.setBounds(80, 140, 120, 120);
		frame.getContentPane().add(lblImageUser);

		/*
		 * LBL Section
		 */
		JLabel lblSection = new JLabel("New label");
		lblSection = new JLabel(section);
		lblSection.setBorder(null);
		lblSection.setBounds(40, 100, 1200, 600);
		frame.getContentPane().add(lblSection);

		/*
		 * Footer Bar
		 */
		JLabel lblFooterBar = new JLabel("Copyright @2017 - BOLT");
		lblFooterBar = new JLabel(footerBar);
		lblFooterBar.setForeground(Color.WHITE);
		lblFooterBar.setHorizontalTextPosition(SwingConstants.CENTER);
		lblFooterBar.setText("Copyright 2017 - BOLT");
		lblFooterBar.setBorder(null);
		lblFooterBar.setBounds(0, 760, 1280, 40);
		frame.getContentPane().add(lblFooterBar);

		/*
		 * LBL Sfondo
		 */
		JLabel lblSfondo = new JLabel("New label");
		lblSfondo = new JLabel(sfondo);
		lblSfondo.setBorder(null);
		lblSfondo.setBounds(0, 0, 1280, 800);
		frame.getContentPane().add(lblSfondo);

		/*
		 * Pulsante Modifica
		 */
		btnEdit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				/*
				 * Apre un nuovo pannello.
				 */
				txtNome.setEnabled(true);
				txtCognome.setEnabled(true);
				txtNick.setEnabled(true);
				txtEmail.setEnabled(false);
				txtPwd.setEnabled(true);
				txtConfermaPwd.setEnabled(true);
				btnOk.setEnabled(true);
				btnOk.setVisible(true);
				frame.validate();
				frame.repaint();
				vecchiaEmail = txtEmail.getText();

			}
		});

		/*
		 * Pulsante Conferma
		 */
		btnOk.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("deprecation")
			@Override
			public void mouseClicked(MouseEvent e) {

				/*
				 * Apre un nuovo pannello.
				 */
				txtNome.setEnabled(false);
				txtCognome.setEnabled(false);
				txtNick.setEnabled(false);
				txtEmail.setEnabled(false);
				txtPwd.setEnabled(false);
				txtConfermaPwd.setEnabled(false);
				btnOk.setEnabled(false);
				btnOk.setVisible(false);
				frame.validate();
				frame.repaint();

				if (txtConfermaPwd.getText().equals("Conferma Password")) {
					if (controller.modificaParametri(txtNome.getText(), txtCognome.getText(), txtNick.getText(),
							txtEmail.getText())) {
						//Alert modifica dati andata a buon fine. Le modifiche saranno visibili al prossimo accesso
						JOptionPane.showMessageDialog(null,
								"La modifica e' andata a buon fine! Le modifiche saranno visibili al prossimo accesso.");
					} else {
						//Alert modifica dati NON andata a buon fine
						JOptionPane.showMessageDialog(null, "La modifica non e' andata a buon fine!");
					}
				} else {
					if (txtPwd.getText().equals(txtConfermaPwd.getText())) {
						if (controller.modificaParametriConPsw(txtNome.getText(), txtCognome.getText(),
								txtNick.getText(), txtEmail.getText(), txtPwd.getText())) {
							//Alert modifica dati andata a buon fine
							JOptionPane.showMessageDialog(null, "La modifica dei dati e' andata a buon fine!");
						} else {
							//Alert modifica dati NON andata a buon fine
							JOptionPane.showMessageDialog(null, "La modifica dei dati non e' andata a buon fine!");
						}
					} else {
						//Alert password non uguali
						JOptionPane.showMessageDialog(null, "Le Password non corrispondono!");
					}
				}
			}
		});

		/*
		 * Pulsante Back
		 */
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});

		/*
		 * Pulsante Elimina
		 */
		btnElimina.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				btnElimina.setBackground(new Color(231, 76, 60));
			}

			public void mouseExited(MouseEvent me) {
				btnElimina.setBackground(new Color(255, 193, 80));
			}
		});
		btnElimina.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Ho Eliminato " + commentiModeratore.getSelectedValue());
				commentiModeratore.setSelectionBackground(new Color(255, 87, 34));
				int elemento = commentiModeratore.getSelectedIndex();
				String titleReview = (String) commentiModeratore.getSelectedValue();

				moderController.eliminaReview(reviewTree.get(titleReview));
				reviewTree.remove(titleReview);
				arrayTitle[elemento] = "";
				commentiModeratore.validate();
				commentiModeratore.repaint();

			}
		});

		/*
		 * Pulsante Approva.
		 */
		btnApprova.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				btnApprova.setBackground(new Color(46, 204, 113));
			}

			public void mouseExited(MouseEvent me) {
				btnApprova.setBackground(new Color(255, 193, 80));
			}
		});
		btnApprova.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Ho Approvato " + commentiModeratore.getSelectedValue());
				commentiModeratore.setSelectionBackground(new Color(76, 175, 80));
				int index = commentiModeratore.getSelectedIndex();
				String titleReview = (String) commentiModeratore.getSelectedValue();
				moderController.approvaRecensione(reviewTree.get(titleReview));
				reviewTree.remove(titleReview);
				arrayTitle[index] = "";
				commentiModeratore.validate();
				commentiModeratore.repaint();
			}
		});

		/*
		 * Svuota i Campi al Click.
		 */

		txtNome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaNome == false)
					txtNome.setText("");
				svuotaNome = true;
			}
		});

		txtCognome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaCognome == false)
					txtCognome.setText("");
				svuotaCognome = true;
			}
		});

		txtNick.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaNick == false)
					txtNick.setText("");
				svuotaNick = true;
			}
		});

		txtEmail.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaEmail == false)
					txtEmail.setText("");
				svuotaEmail = true;
			}
		});

		txtPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtPwd.setBackground(new Color(224, 224, 224, 150));
				txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
				txtPwd.setEchoChar('*');
				if (svuotaPwd == false)
					txtPwd.setText("");
				svuotaPwd = true;
			}
		});

		txtConfermaPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtPwd.setBackground(new Color(224, 224, 224, 150));
				txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
				txtConfermaPwd.setEchoChar('*');
				if (svuotaConfermaPwd == false)
					txtConfermaPwd.setText("");
				svuotaConfermaPwd = true;
			}
		});

		/*
		 * Pulsante Timeline
		 */
		btnTimeline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTimeline.setBackground(new Color(255, 160, 0));
				btnModerate.setBackground(new Color(255, 193, 80));
				timelineModeratore.setVisible(true);
				commentiModeratore.setVisible(false);
				btnApprova.setVisible(false);
				btnElimina.setVisible(false);
				lblCommento.setVisible(false);
				lblCommento.setOpaque(false);
				frame.validate();
				frame.repaint();
			}
		});

		/*
		 * Pulsante Moderatore
		 */
		btnModerate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTimeline.setBackground(new Color(255, 193, 80));
				btnModerate.setBackground(new Color(255, 160, 0));
				timelineModeratore.setVisible(false);
				commentiModeratore.setVisible(true);
				btnApprova.setVisible(true);
				btnElimina.setVisible(true);
				lblCommento.setVisible(true);
				lblCommento.setOpaque(true);
				frame.validate();
				frame.repaint();
			}
		});
	}

	/**
	 * Verifica il tipo di utente che sta visualizzando il proprio profilo.
	 * 
	 * @param tipoUser Tipo di utente.
	 */
	public void verificaUser(String tipoUser) {

		if (tipoUser.equals("Gamer")) {
			System.out.println("user");
			btnTimeline.setVisible(false);
			btnModerate.setVisible(false);
			btnApprova.setVisible(false);
			btnElimina.setVisible(false);
			btnApprova.setEnabled(false);
			btnElimina.setEnabled(false);
			btnTimeline.setEnabled(false);
			btnModerate.setEnabled(false);
			timelineGamer.setVisible(true);
			lblCommento.setVisible(false);
			lblCommento.setOpaque(false);
		} else if (tipoUser.equals("Moderator")) {
			System.out.println("Moderator");
			lblTimeline.setVisible(false);
			timelineGamer.setVisible(false);
		}

	}

	/**
	 * Cambia il numero di stelle in base al livello.
	 * 
	 * @param punteggio Punti esperienza guadagnati dall'utente.
	 */
	public void cambiaStella(int punteggio) {

		if (punteggio == LIVELLO_0) {
			star0 = "resources/img/profileView/starBlack.png";
			star1 = "resources/img/profileView/starBlack.png";
			star2 = "resources/img/profileView/starBlack.png";
			star3 = "resources/img/profileView/starBlack.png";
			star4 = "resources/img/profileView/starBlack.png";
		} else if ((punteggio > LIVELLO_0) && (punteggio <= LIVELLO_2)) {
			star0 = "resources/img/profileView/starYellow.png";
			star1 = "resources/img/profileView/starBlack.png";
			star2 = "resources/img/profileView/starBlack.png";
			star3 = "resources/img/profileView/starBlack.png";
			star4 = "resources/img/profileView/starBlack.png";
		} else if ((punteggio > LIVELLO_2) && (punteggio <= LIVELLO_4)) {
			star0 = "resources/img/profileView/starYellow.png";
			star1 = "resources/img/profileView/starYellow.png";
			star2 = "resources/img/profileView/starBlack.png";
			star3 = "resources/img/profileView/starBlack.png";
			star4 = "resources/img/profileView/starBlack.png";
		} else if ((punteggio > LIVELLO_4) && (punteggio <= LIVELLO_6)) {
			star0 = "resources/img/profileView/starYellow.png";
			star1 = "resources/img/profileView/starYellow.png";
			star2 = "resources/img/profileView/starYellow.png";
			star3 = "resources/img/profileView/starBlack.png";
			star4 = "resources/img/profileView/starBlack.png";
		} else if ((punteggio > LIVELLO_6) && (punteggio <= LIVELLO_8)) {
			star0 = "resources/img/profileView/starYellow.png";
			star1 = "resources/img/profileView/starYellow.png";
			star2 = "resources/img/profileView/starYellow.png";
			star3 = "resources/img/profileView/starYellow.png";
			star4 = "resources/img/profileView/starBlack.png";
		} else if (punteggio > LIVELLO_8) {
			star0 = "resources/img/profileView/starYellow.png";
			star1 = "resources/img/profileView/starYellow.png";
			star2 = "resources/img/profileView/starYellow.png";
			star3 = "resources/img/profileView/starYellow.png";
			star4 = "resources/img/profileView/starYellow.png";
		}

	}

}
