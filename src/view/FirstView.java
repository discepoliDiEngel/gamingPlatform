package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.JOptionPane;

import view.controller.FirstViewController;

/**
 * Prima finestra di interazione con la piattaforma da cui un utente si puo' registrare oppure
 * effettuare l'accesso alla piattaforma.
 * 
 * @author Discepoli di Engel
 *
 */
public class FirstView extends Thread {

	private JFrame frame;
	private JTextField txtNome;
	private JTextField txtCognome;
	private JTextField txtEmail;
	private JTextField txtNick;
	private JTextField txtPwd;
	private JTextField txtConfermaPwd;
	private JTextField txtEmailNick;
	private JTextField txtLoginPwd;
	private FirstViewController fvController;

	/**
	 * Costruisce una nuova finestra di accesso alla piattaforma invocata dal controller. 
	 * 
	 * @param fvc Controller della finestra.
	 */
	public FirstView() {
		
		fvController = new FirstViewController(this);
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * Servono per non far svuotare i campi piu' volte.
	 */
	boolean svuotaNome = false;
	boolean svuotaCognome = false;
	boolean svuotaNick = false;
	boolean svuotaEmail = false;
	boolean svuotaPwd = false;
	boolean svuotaConfermaPwd = false;
	boolean svuotaEmailNick = false;
	boolean svuotaLoginPwd = false;

	/*
	 * Servono a passare le stringhe al DB.
	 */
	private String nome = "";
	private String cognome = "";
	private String email = "";
	private String password = "";
	private String emailLogin = "";
	private String passwordLogin = "";
	private String nick = "";

	/*
	 * Dati alert.
	 */
	static int selettoreAlert;
	static String messaggioAlert = "";

	/**
	 * Inizializza la prima finestra di benvenuto con cui interagisce l'utente.
	 * Permette di eseguire l'accesso o la registrazione sulla piattaforma.
	 */
	private void initialize() {

		JButton btnRegistrati = new JButton("Registrati");
		ImageIcon sfondo = new ImageIcon("resources/img/wallpaper/WallLoginPass.jpg");
		ImageIcon section = new ImageIcon("resources/img/firstView/Section.png");
		ImageIcon key = new ImageIcon("resources/img/firstView/key.png");
		ImageIcon footerBar = new ImageIcon("resources/img/admin/FooterBar.png");
		ImageIcon topBar = new ImageIcon("resources/img/admin/TitleBar.png");
		ImageIcon topBarBottom = new ImageIcon("resources/img/admin/TitleBarBottom.png");
		ImageIcon logo = new ImageIcon("resources/img/admin/boltLogo.png");

		/*
		 * Istanziazione frame.
		 */
		frame = new JFrame();
		frame.setTitle("BOLT Gaming Platform - Register|Login");
		frame.setBounds(100, 0, 1280, 820);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false); // impedisce il ridimensionamento della finestra
		frame.setLocationRelativeTo(null); // mette al centro la finestra
		UIManager.put("OptionPane.background", Color.white);
		UIManager.put("Panel.background", Color.white);
		UIManager.put("OptionPane.messagebackground", Color.BLUE);

		/*
		 * Bottone relativo all'errore
		 */
		JButton okButton = new JButton("OK");
		okButton.setBackground(Color.gray);
		okButton.setForeground(Color.white);
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				JOptionPane.getRootFrame().dispose();
			}
		});

		/*
		 * Logo Bolt
		 */
		JLabel lblLogo = new JLabel("New label");
		lblLogo = new JLabel(logo);
		lblLogo.setBorder(null);
		lblLogo.setBounds(580, 0, 120, 58);
		frame.getContentPane().add(lblLogo);

		/*
		 * Top Bar
		 */
		JLabel lblTopBar = new JLabel("New label");
		lblTopBar = new JLabel(topBar);
		lblTopBar.setBorder(null);
		lblTopBar.setBounds(0, 0, 1280, 60);
		frame.getContentPane().add(lblTopBar);

		/*
		 * Top Bar 2
		 */
		JLabel lblTopBarBottom = new JLabel("New label");
		lblTopBarBottom = new JLabel(topBarBottom);
		lblTopBarBottom.setBorder(null);
		lblTopBarBottom.setBounds(0, 60, 1280, 4);
		frame.getContentPane().add(lblTopBarBottom);

		/*
		 *  Label - Registrati
		 */
		JLabel lblRegistrati = new JLabel("Registrati");
		lblRegistrati.setForeground(Color.WHITE);
		lblRegistrati.setFont(new Font("Menlo", Font.PLAIN, 14));
		lblRegistrati.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistrati.setBounds(290, 120, 300, 30);
		frame.getContentPane().add(lblRegistrati);

		/*
		 *  Label - Login
		 */
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setFont(new Font("Menlo", Font.PLAIN, 14));
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setBounds(690, 120, 300, 30);
		frame.getContentPane().add(lblLogin);

		// Sezione registrazione //

		/*
		 *  TextField - Nome
		 */
		txtNome = new JTextField();
		txtNome.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtNome.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtNome.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtNome.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtNome.setForeground(Color.WHITE);
		txtNome.setBackground(new Color(224, 224, 224, 150));
		txtNome.setBounds(290, 160, 300, 40);
		txtNome.setBorder(null);
		txtNome.setBorder(
				BorderFactory.createCompoundBorder(txtNome.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtNome);
		txtNome.setText("Nome");

		/*
		 * TextField - Cognome
		 */
		txtCognome = new JTextField();
		txtCognome.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtCognome.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtCognome.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtCognome.setForeground(Color.WHITE);
		txtCognome.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtCognome.setBackground(new Color(224, 224, 224, 150));
		txtCognome.setBounds(290, 220, 300, 40);
		txtCognome.setBorder(null);
		txtCognome.setBorder(BorderFactory.createCompoundBorder(txtCognome.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtCognome);
		txtCognome.setText("Cognome");

		/*
		 * TextField - Nick
		 */
		txtNick = new JTextField();
		txtNick.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtNick.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtNick.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtNick.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtNick.setForeground(Color.WHITE);
		txtNick.setBackground(new Color(224, 224, 224, 150));
		txtNick.setBounds(290, 280, 300, 40);
		txtNick.setBorder(null);
		txtNick.setBorder(
				BorderFactory.createCompoundBorder(txtNick.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtNick);
		txtNick.setText("Nick");

		/*
		 * TextField - Email
		 */
		txtEmail = new JTextField();
		txtEmail.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtEmail.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtEmail.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtEmail.setForeground(Color.WHITE);
		txtEmail.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtEmail.setBackground(new Color(224, 224, 224, 150));
		txtEmail.setBounds(290, 340, 300, 40);
		txtEmail.setBorder(null);
		txtEmail.setBorder(
				BorderFactory.createCompoundBorder(txtEmail.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtEmail);
		txtEmail.setText("Email");

		/*
		 * Key Password
		 */
		JLabel lblKey1 = new JLabel("New label");
		lblKey1 = new JLabel(key);
		lblKey1.setBorder(null);
		lblKey1.setBounds(556, 408, 24, 24);
		frame.getContentPane().add(lblKey1);

		/*
		 * Key Password
		 */
		JLabel lblKey2 = new JLabel("New label");
		lblKey2 = new JLabel(key);
		lblKey2.setBorder(null);
		lblKey2.setBounds(556, 468, 24, 24);
		frame.getContentPane().add(lblKey2);

		/*
		 * Key Password
		 */
		JLabel lblKey3 = new JLabel("New label");
		lblKey3 = new JLabel(key);
		lblKey3.setBorder(null);
		lblKey3.setBounds(956, 228, 24, 24);
		frame.getContentPane().add(lblKey3);

		/*
		 * TextField - Password
		 */
		JPasswordField txtPwd = new JPasswordField(20);
		txtPwd.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtPwd.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtPwd.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtPwd.setForeground(Color.WHITE);
		txtPwd.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtPwd.setBackground(new Color(224, 224, 224, 150));
		txtPwd.setBounds(290, 400, 300, 40);
		txtPwd.setBorder(null);
		txtPwd.setEchoChar((char) 0);
		txtPwd.setBorder(
				BorderFactory.createCompoundBorder(txtPwd.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtPwd);
		txtPwd.setText("Password");

		/*
		 * TextField - Conferma Password
		 */
		JPasswordField txtConfermaPwd = new JPasswordField(20);
		txtConfermaPwd.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtConfermaPwd.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtConfermaPwd.setForeground(Color.WHITE);
		txtConfermaPwd.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
		txtConfermaPwd.setBounds(290, 460, 300, 40);
		txtConfermaPwd.setBorder(null);
		txtConfermaPwd.setEchoChar((char) 0);
		txtConfermaPwd.setBorder(BorderFactory.createCompoundBorder(txtConfermaPwd.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtConfermaPwd);
		txtConfermaPwd.setText("Conferma Password");

		// Fine Sezione Registrati //

		// Inizio Sezione Login //

		/*
		 * TextField - Cognome
		 */
		txtEmailNick = new JTextField();
		txtEmailNick.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtEmailNick.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtEmailNick.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtEmailNick.setForeground(Color.WHITE);
		txtEmailNick.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtEmailNick.setBackground(new Color(224, 224, 224, 150));
		txtEmailNick.setBounds(690, 160, 300, 40);
		txtEmailNick.setBorder(null);
		txtEmailNick.setBorder(BorderFactory.createCompoundBorder(txtEmailNick.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtEmailNick);
		txtEmailNick.setText("Inserisci la tua Email");

		/*
		 * TextField - Password
		 */
		JPasswordField txtLoginPwd = new JPasswordField(20);
		txtLoginPwd.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				txtLoginPwd.setBackground(new Color(52, 152, 219));
			}

			public void mouseExited(MouseEvent me) {
				txtLoginPwd.setBackground(new Color(224, 224, 224, 150));
			}
		});
		txtLoginPwd.setForeground(Color.WHITE);
		txtLoginPwd.setFont(new Font("Menlo", Font.PLAIN, 14));
		txtLoginPwd.setBackground(new Color(224, 224, 224, 150));
		txtLoginPwd.setBounds(690, 220, 300, 40);
		txtLoginPwd.setBorder(null);
		txtLoginPwd.setEchoChar((char) 0);
		txtLoginPwd.setBorder(BorderFactory.createCompoundBorder(txtLoginPwd.getBorder(),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		frame.getContentPane().add(txtLoginPwd);
		txtLoginPwd.setText("Password");

		txtPwd.setEchoChar((char) 0);
		txtConfermaPwd.setEchoChar((char) 0);
		txtLoginPwd.setEchoChar((char) 0);

		// Fine sezione login

		/*
		 * Checkbox: Mostra-Nascondi Password
		 */
		JCheckBox chckbxMostraPwd = new JCheckBox("Show Password");
		chckbxMostraPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (chckbxMostraPwd.isSelected()) {
					txtPwd.setEchoChar('*');
					txtConfermaPwd.setEchoChar('*');
					txtLoginPwd.setEchoChar('*');
					System.out.println("Hai Nascosto la Password " + String.valueOf(txtPwd.getPassword()));
				} else {
					txtPwd.setEchoChar((char) 0);
					txtConfermaPwd.setEchoChar((char) 0);
					txtLoginPwd.setEchoChar((char) 0);
					System.out.println("Hai Mostrato la Password " + String.valueOf(txtPwd.getPassword()));
				}
			}
		});
		chckbxMostraPwd.setBounds(290, 520, 150, 23);
		chckbxMostraPwd.setForeground(Color.WHITE);
		frame.getContentPane().add(chckbxMostraPwd);

		/*
		 * Btn Registrati
		 */
		btnRegistrati.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent me) {
				btnRegistrati.setBackground(new Color(46, 204, 113));
			}

			public void mouseExited(MouseEvent me) {
				btnRegistrati.setBackground(new Color(2, 136, 209));
			}
		});
		btnRegistrati.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//Se tutti i campi della sezione registrati sono vuoti, invoca alert 0: tutti i campi sono vuoti.
				if (txtNome.getText().equals("") | txtCognome.getText().equals("") | txtEmail.getText().equals("")
						| txtNick.getText().equals("") | String.valueOf(txtPwd.getPassword()).equals("")
						| String.valueOf(txtConfermaPwd.getPassword()).equals("")) {
					selettoreAlert = 0;
					switchAlert();
					System.out.println("Uno dei campi Registrazione e' vuoto.");
				} else { // Se il contenuto dei campi delle due password sono uguali, prosegue con la registrazione.
					if (String.valueOf(txtConfermaPwd.getPassword()).equals(String.valueOf(txtPwd.getPassword()))) {
						if (txtEmail.getText().contains("@")) {
							System.out.println("RIGA:355 - Le password inserite sono uguali.");
							email = txtEmail.getText();
							nome = txtNome.getText();
							cognome = txtCognome.getText();
							nick = txtNick.getText();
							password = String.valueOf(txtPwd.getPassword());
							switch (fvController.register(nick, email, nome, cognome, password)) {
							case 11: // Utente gia' registrato
								selettoreAlert = 11;
								switchAlert();
								break;
							case 12: // Registrazione completata
								selettoreAlert = 12;
								switchAlert();
								break;
							case 13: // Registrazione fallita
								selettoreAlert = 13;
								switchAlert();
								break;
							}
							System.out.println("Le password sono uguali");
						} else {
							selettoreAlert = 7;
							switchAlert();
						}
					}
					// Altrimenti invoca alert: le due password inserite non corrispondono.
					else {
						txtPwd.setBackground(new Color(255, 87, 34, 150));
						txtConfermaPwd.setBackground(new Color(255, 87, 34, 150));
						selettoreAlert = 3;
						switchAlert();
						System.out.println("Le password non corrispondono");
					}
				}
			}
		});
		btnRegistrati.setForeground(Color.WHITE);
		btnRegistrati.setFont(new Font("Menlo", Font.PLAIN, 18));
		btnRegistrati.setBorderPainted(false);
		btnRegistrati.setBackground(new Color(2, 136, 209));
		btnRegistrati.setOpaque(true);
		btnRegistrati.setBounds(340, 640, 200, 40);
		frame.getContentPane().add(btnRegistrati);

		/*
		 * Btn Login
		 */
		JButton btnLogin = new JButton("Login");

		btnLogin.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent me) {
				btnLogin.setBackground(new Color(46, 204, 113));
			}

			public void mouseExited(MouseEvent me) {
				btnLogin.setBackground(new Color(2, 136, 209));
			}

		});

		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Se i campi email e password sono vuoti, invoca il metodo alert.
				if (txtEmailNick.getText().equals("") | txtLoginPwd.getPassword().toString().equals("")) {
					txtLoginPwd.setBackground(new Color(255, 87, 34, 150));
					selettoreAlert = 0;
					switchAlert();
				} else {
					//Altrimenti, prosegue con il Login.
					emailLogin = txtEmailNick.getText();
					passwordLogin = String.valueOf(txtLoginPwd.getPassword());
					System.out.println("Login: " + emailLogin + " " + passwordLogin);
					
					switch (fvController.login(emailLogin, passwordLogin)) {

					case 0: //Caricamento eseguito in modo corretto
						new GamerView(fvController.getUser());
						frame.setVisible(false);
						frame.dispose();
						System.out.println("case 0");
						break;

					case 1: //Caricamento eseguito in modo corretto
						new GamerView(fvController.getUser());
						frame.setVisible(false);
						frame.dispose();
						System.out.println("case 1");
						break;

					case 2: //Caricamento eseguito in modo corretto
						new AdminView();
						frame.setVisible(false);
						frame.dispose();
						System.out.println("case 2");
						break;

					case 9: //email e password non corrispondono
						selettoreAlert = 9;
						switchAlert();
						System.out.println("case 9");
						break;

					case 10: //email non trovata
						selettoreAlert = 10;
						switchAlert();
						System.out.println("case 10");
						break;
					} // fine switch
					
				}
				
			}
		});
		btnLogin.setFont(new Font("Menlo", Font.PLAIN, 16));
		btnLogin.setForeground(Color.WHITE);
		btnLogin.setBounds(740, 640, 200, 40);
		btnLogin.setBorderPainted(false);
		btnLogin.setBackground(new Color(2, 136, 209));
		btnLogin.setOpaque(true);
		frame.getContentPane().add(btnLogin);

		/*
		 * Inizio Section
		 */
		JLabel lblSection = new JLabel("New label");
		lblSection = new JLabel(section);
		lblSection.setBorder(null);
		lblSection.setBounds(240, 100, 800, 600);
		frame.getContentPane().add(lblSection);

		/*
		 * Footer Bar
		 */
		JLabel lblFooterBar = new JLabel("Copyright @2017 - BOLT");
		lblFooterBar = new JLabel(footerBar);
		lblFooterBar.setForeground(Color.WHITE);
		lblFooterBar.setHorizontalTextPosition(SwingConstants.CENTER);
		lblFooterBar.setText("Copyright 2017 - BOLT");
		lblFooterBar.setBorder(null);
		lblFooterBar.setBounds(0, 760, 1280, 40);
		frame.getContentPane().add(lblFooterBar);

		JLabel lblSfondo = new JLabel("New label");
		lblSfondo = new JLabel(sfondo);
		lblSfondo.setBorder(null);
		lblSfondo.setBounds(0, 0, 1280, 800);
		frame.getContentPane().add(lblSfondo);

		/*
		 * Mouse listeners per svuotare i campi.
		 */
		txtNome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaNome == false)
					txtNome.setText("");
				svuotaNome = true;
			}
		});

		txtCognome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaCognome == false)
					txtCognome.setText("");
				svuotaCognome = true;
			}
		});

		txtNick.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaNick == false)
					txtNick.setText("");
				svuotaNick = true;
			}
		});

		txtEmail.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaEmail == false)
					txtEmail.setText("");
				svuotaEmail = true;
			}
		});

		txtPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtPwd.setBackground(new Color(224, 224, 224, 150));
				txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
				txtPwd.setEchoChar('*');
				if (svuotaPwd == false)
					txtPwd.setText("");
				svuotaPwd = true;
			}
		});

		txtConfermaPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtPwd.setBackground(new Color(224, 224, 224, 150));
				txtConfermaPwd.setBackground(new Color(224, 224, 224, 150));
				txtConfermaPwd.setEchoChar('*');
				if (svuotaConfermaPwd == false)
					txtConfermaPwd.setText("");
				svuotaConfermaPwd = true;
			}
		});

		txtEmailNick.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (svuotaEmailNick == false)
					txtEmailNick.setText("");
				svuotaEmailNick = true;
			}
		});

		txtLoginPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtLoginPwd.setBackground(new Color(224, 224, 224, 150));
				txtLoginPwd.setEchoChar('*');
				if (svuotaLoginPwd == false)
					txtLoginPwd.setText("");
				svuotaLoginPwd = true;
			}
		});
	}

	/**
	 * Seleziona il tipo di alert da restituire.
	 */
	public static void switchAlert() {
		switch (selettoreAlert) {
		case 0:
			messaggioAlert = "Devi riempire tutti i campi.";
			alert();
			break;
		case 9:
			messaggioAlert = "Email e Password non corrispondono.";
			alert();
			break;
		case 10:
			messaggioAlert = "Email non trovata.";
			alert();
			break;
		case 11:
			messaggioAlert = "Email gia' esistente!";
			alert();
			break;
		case 3:
			messaggioAlert = "Le Password non corrispondono.";
			alert();
			break;
		case 7:
			messaggioAlert = "Controlla il formato della Mail.";
			alert();
			break;
		case 12:
			messaggioAlert = "La registrazionie e' stata effettuata. \n Ora effettua il Login";
			alert();
			break;
		case 13:
			messaggioAlert = "La registrazionie NON e' stata effettuata. \n Riprova";
			alert();
			break;
		default:
			messaggioAlert = "Errore!";
		}
	}

	/**
	 * Invoca l'alert da restituire all'utente.
	 */
	public static void alert() {
		JFrame parent = new JFrame();
		JOptionPane.showMessageDialog(parent, messaggioAlert);
		System.out.println("Alcuni campi erano vuoti");
	}

	/**
	 * @return the txtPwd
	 */
	public JTextField getTxtPwd() {
		return txtPwd;
	}

	/**
	 * @param txtPwd the txtPwd to set
	 */
	public void setTxtPwd(JTextField txtPwd) {
		this.txtPwd = txtPwd;
	}

	/**
	 * @return the txtConfermaPwd
	 */
	public JTextField getTxtConfermaPwd() {
		return txtConfermaPwd;
	}

	/**
	 * @param txtConfermaPwd the txtConfermaPwd to set
	 */
	public void setTxtConfermaPwd(JTextField txtConfermaPwd) {
		this.txtConfermaPwd = txtConfermaPwd;
	}

	/**
	 * @return the txtLoginPwd
	 */
	public JTextField getTxtLoginPwd() {
		return txtLoginPwd;
	}

	/**
	 * @param txtLoginPwd the txtLoginPwd to set
	 */
	public void setTxtLoginPwd(JTextField txtLoginPwd) {
		this.txtLoginPwd = txtLoginPwd;
	}

}
