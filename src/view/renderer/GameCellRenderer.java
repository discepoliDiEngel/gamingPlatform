package view.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import model.Game;

/**
 * Renderer per l'oggetto Game inserito in un oggetto JList.
 * @author Discepoli di Engel
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
public class GameCellRenderer extends JLabel implements ListCellRenderer {
	  
	private static final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

	  public GameCellRenderer() {
	    setOpaque(true);
	  }

	  public Component getListCellRendererComponent(JList list, Object value,
	      int index, boolean isSelected, boolean cellHasFocus) {
		  
	    Game entry = (Game) value;
	    
	    setText(entry.getName());
	    
	    if (isSelected) {
	    	
	      setBackground(HIGHLIGHT_COLOR);
	      setForeground(Color.white);
	      
	    } else {
	    	
	      setBackground(Color.white);
	      setForeground(Color.black);
	      
	    }
	    
	    return this;
	    
	  }
	  
}