package view.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import model.User;

/**
 * Renderer per l'oggetto User inserito in un oggetto JList.
 * 
 * @author Discepoli di Engel
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
public class UserCellRenderer extends JLabel implements ListCellRenderer {
	  
	private Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

	  public UserCellRenderer() {
	    setOpaque(true);
	  }

	  public Component getListCellRendererComponent(JList list, Object value,
	      int index, boolean isSelected, boolean cellHasFocus) {
		  
	    User entry = (User) value;
	    
	    setText(entry.getUsername());
	    
	    if (isSelected) {
	    	
	      setBackground(HIGHLIGHT_COLOR);
	      setForeground(Color.white);
	      
	    } else {
	    	
	      setBackground(Color.white);
	      setForeground(Color.black);
	      
	    }
	    
	    return this;
	    
	  }
	  
}
