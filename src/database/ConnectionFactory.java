package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 
 * @author Discepoli di Engel
 *
 */
public class ConnectionFactory {

	private static Connection con;

	// Se si decide di cambiare il tipo di database basta modificare i seguenti attributi
	private static String DATABASE_DRIVER = "jdbc";
	private static String DATABASE_SERVICE = "mysql";
	private static String DATABASE_HOST = "127.0.0.1";
	private static String DATABASE_NAME = "provagaming";
	private static String DATABASE_USER = "root";
	private static String DATABASE_PASSWORD = "";

	/**
	 * Stabilisce una connessione con il database.
	 * 
	 * @return boolean true: connessione con database avvenuta con successo, false altrimenti
	 */
	public boolean connectToDb() {

		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection(DATABASE_DRIVER + ":" + DATABASE_SERVICE + "://"
					+ DATABASE_HOST+ "/" + DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD);
			
			if (con != null) {
				System.out.println("You made it, take control your database now!");
			} else 
				System.out.println("Failed to make connection!");

		} catch (ClassNotFoundException e) {
			
			System.err.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return false;
			
		} catch(SQLException e) {
			
			System.err.println("Connection Failed! Check output console.");
			e.printStackTrace();
			return false;
			
		}

		return true;
	}

	/**
	 * Restituisce l'oggetto Connection necessario per poter comunicare con il Database.
	 * 
	 * @return Connection
	 */
	public Connection getConnection() {
		if(con == null) connectToDb();
		return con;
	}

	/**
	 * Chiude la connessione con il database.
	 * 
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		con.close();
	}
	
}