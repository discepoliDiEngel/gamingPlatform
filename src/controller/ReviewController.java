package controller;

import java.util.TreeMap;

import model.Game;
import model.Review;
import model.dao.concrete.ReviewDaoImpl;
import model.dao.interfaces.ReviewDao;

public class ReviewController {

	Review review;
	ReviewDao reviewDao = new ReviewDaoImpl();
	private TreeMap<String, Review> reviewTree;
	private String[] titles;
	
	public ReviewController(){
		reviewTree = reviewDao.mapReview();
		titles = new String[reviewTree.size()];
		
		int i = 0;

		// Carica l'array di stringhe per passare solo i titoli delle review
		for (String title : reviewTree.keySet()) {
			titles[i] = title;
			i++;
		}
	}
	
	/**
	 * Permette di scrivere una nuova recensione per un gioco.
	 * 
	 * @param title Titolo recensione.
	 * @param author Autore recensione.
	 * @param text Testo recensione.
	 * @param vote Voto recensione.
	 * @param gameID Id gioco per cui e' stata scritta la recensione.
	 */
	public void writeReview(String title, String author, String text, int vote, int gameID) {
		reviewDao.insertReview(title, author, text, vote, gameID);
	}
	
	/**
	 * Carica le recensioni per un determinato gioco.
	 * 
	 * @param game Gioco per cui vengono caricate le recensioni.
	 */
	public TreeMap<String, Review> visualizzaRecensioni(Game game) {
		return reviewDao.getReviewsByGame(game);
	}

	/**
	 * Approva una recensione e lo comunica al Db. 
	 * 
	 * @param rec Review
	 */
	public void approvaRecensione(Review rec) {
		rec.setApproved(true);
		reviewDao.approveReview(rec);
	}

	/**
	 * Elimina una recensione dal database.
	 * 
	 * @param review
	 */
	public void eliminaReview(Review review) {
		reviewDao.deleteReview(review);
		reviewTree.remove(review.getTitle());
	}
	
	
	/**
	 * @return the users
	 */
	public String[] getTitles() {
		return titles;
	}

	/**
	 * @param users the users to set
	 */
	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	/**
	 * @return the reviewTree
	 */
	public TreeMap<String, Review> getReviewTree() {
		return reviewTree;
	}

	/**
	 * @param map to set to reviewTree
	 */
	public void setReviewTree(TreeMap<String, Review> map) {
		reviewTree = map;
	}
}
