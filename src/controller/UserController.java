package controller;

import java.util.ArrayList;
import java.util.Date;

import model.Game;
import model.User;
import model.dao.concrete.UserDaoImpl;
import model.dao.interfaces.UserDao;

public class UserController {

	User gamer;
	UserDao userDao = new UserDaoImpl();
	private ArrayList<User> usersList;
	private User[] users;
	

	public UserController(){
		usersList = userDao.listUser();
		users = usersList.toArray(new User[usersList.size()]);
	}
	
	public UserController(User gamer){
		this.gamer = gamer;
		usersList = userDao.listUser();
		users = usersList.toArray(new User[usersList.size()]);
	}
	
	public int login(String email, String password) {

		if(userDao.userExist(email)){
			gamer = userDao.getUser(email);
			System.out.println(gamer.toString());
			return userDao.login(email, password);
		}
		
		return 10;

	}
	
	/**
	 * Registra un nuovo oggetto User sulla piattaforma, se non esiste nel DB.
	 * 
	 * @param email e-mail utente
	 * @param nome Nome utente
	 * @param cognome Cognome utente
	 * @param username Username utente
	 * @param password Password utente
	 * @return int 
	 */
	public int register( String username, String email, String nome, String cognome, String password) {

		if(userDao.userExist(email)) {
			return 11;
		} else {

			if(userDao.register(username, email, nome, cognome, password))
				return 12;
			else
				return 13;
			
		}
		
	}
	
	
	/**
	 * Aggiorna i dati dell'utente.
	 * 
	 * @param nome Nome utente.
	 * @param cognome Cognome utente.
	 * @param username Username utente.
	 * @param email E-mail utente.
	 * @return boolean
	 */
	public boolean modificaParametri(String nome, String cognome, String username, String email) {
		return userDao.editUserInfo(nome, cognome, email, email, username);
	}
	
	/**
	 * Aggiorna i dati dell'utente compresa la password.
	 * 
	 * @param nome Nome utente.
	 * @param cognome Cognome utente.
	 * @param username Username utente.
	 * @param email E-mail utente.
	 * @param password Nuova password utente.
	 * @return boolean
	 */
	public boolean modificaParametriConPsw(String nome, String cognome, String username, String email, String password) {
		return userDao.editUserPassword(nome, cognome, email, username, password);
	}

	
	/**
	 * Promuove un utente base al ruolo di utente moderatore.
	 * 
	 * @return boolean
	 */
	public boolean promuovi (User user) {	
		return userDao.promoteUser(user);
	}

	/**
	 * Retrocede un utente da Moderatore a Utente base.
	 * 
	 * @return boolean
	 */
	public boolean retrocedi (User user) {
		return userDao.degradeUser(user);
	}
	
	/**
	 * Elimina un utente dalla piattaforma.
	 * 
	 * @param user Utente da eliminare.
	 * @return boolean
	 */
	public boolean eliminaUtente(User user) {
		return userDao.deleteUser(user.getEmail());
	}
	
	/**
	 * @return the usersList
	 */
	public ArrayList<User> getUsersList() {
		return usersList;
	}

	/**
	 * @param usersList the usersList to set
	 */
	public void setUsersList(ArrayList<User> usersList) {
		this.usersList = usersList;
	}

	/**
	 * @return the u
	 */
	public User getUser() {
		return gamer;
	}

	/**
	 * @param u the u to set
	 */
	public void setUser(User user) {
		this.gamer = user;
	}
	
	/**
	 * @return the users
	 */
	public User[] getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(User[] users) {
		this.users = users;
	}
	
	/**
	 * Restituisce l'esperienza acquisita dal giocatore
	 * 
	 * @return esperienza
	 */
	public int getEsperienza() {
		return gamer.getExperience();
	}
	
	/**
	 * Restituisce il livello del giocatore
	 * 
	 * @return int
	 */
	public int getLivello() {
		return gamer.getExperience() / 100;
	}
	
	/**	
	 * Aggiorna l'esperienza del giocatore con il valore fornito
	 * 
	 * @param esperienza Valore da aggiungere all'esperienza gia' ottenuta.
	 */
	public void setEsperienza(int esperienza) {
		int exp = gamer.getExperience() + esperienza;
		gamer.setExperience(exp);
		userDao.editUserXp(gamer.getEmail(), exp);
	}
	
	/**
	 * Aggiorna la timeline relativa al giocatore nel database.
	 * 
	 * @param game Gioco
	 * @param exp Esperienza ottenuta.
	 */
	public void setTimeline(Game game, int exp) {
		Date data = new Date();
		userDao.updateTimeline(gamer.getEmail(), exp, game, data);
		gamer.setTimeline(recoverTimeline(gamer.getEmail()));
	}

	public ArrayList<String> recoverTimeline(String email) {
		return userDao.getTimeline(email);
	}
}
