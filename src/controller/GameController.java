package controller;

import model.Game;
import model.dao.concrete.GameDaoImpl;

public class GameController {
	
	private Game[] games;
	
	public GameController() {
		games = new GameDaoImpl().getGames();
		
	}
	
	/**
	 * @return the games
	 */
	public Game[] getGames() {
		return games;
	}

}
