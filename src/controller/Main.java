package controller;

import view.FirstView;

/**
 * Classe da cui parte tutto il programma.
 * 
 * @author Discepoli di Engel
 *
 */
public class Main {
	
	public static void main(String[] args) {
		new FirstView();
	}
	
}
